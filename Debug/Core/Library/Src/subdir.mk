################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Library/Src/AT_cmd.c \
../Core/Library/Src/COM_debug.c \
../Core/Library/Src/GNSS_lib.c \
../Core/Library/Src/ansi_console.c \
../Core/Library/Src/flash_presetai.c 

OBJS += \
./Core/Library/Src/AT_cmd.o \
./Core/Library/Src/COM_debug.o \
./Core/Library/Src/GNSS_lib.o \
./Core/Library/Src/ansi_console.o \
./Core/Library/Src/flash_presetai.o 

C_DEPS += \
./Core/Library/Src/AT_cmd.d \
./Core/Library/Src/COM_debug.d \
./Core/Library/Src/GNSS_lib.d \
./Core/Library/Src/ansi_console.d \
./Core/Library/Src/flash_presetai.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Library/Src/%.o: ../Core/Library/Src/%.c Core/Library/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32L431xx -c -I../Core/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/CMSIS/Include -I"D:/Augustinas/Bitbucket/STM/Vaiku_sargis_PCB/Core/Library/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Library-2f-Src

clean-Core-2f-Library-2f-Src:
	-$(RM) ./Core/Library/Src/AT_cmd.d ./Core/Library/Src/AT_cmd.o ./Core/Library/Src/COM_debug.d ./Core/Library/Src/COM_debug.o ./Core/Library/Src/GNSS_lib.d ./Core/Library/Src/GNSS_lib.o ./Core/Library/Src/ansi_console.d ./Core/Library/Src/ansi_console.o ./Core/Library/Src/flash_presetai.d ./Core/Library/Src/flash_presetai.o

.PHONY: clean-Core-2f-Library-2f-Src

