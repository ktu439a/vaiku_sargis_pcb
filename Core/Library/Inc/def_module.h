/*
 * def_module.h
 *
 *  Created on: 2021-07-19
 *      Author: Lituotojas
 */

#ifndef LIBRARY_INC_DEF_MODULE_H_
#define LIBRARY_INC_DEF_MODULE_H_

/*Bateirjos min max*/
#define BATTERY_MV_MAX 4200
#define BATTERY_MV_MIN 3400
/*daugiklis dalikliui*/
#define BATTERY_1V8_T_4V2 7/3

/*Default matrix sizes*/
#define GNSS_RAW_SIZE 255
//#define AT_ANSWER_SIZE 100
#define AT_ANSWER_SIZE 100

/*UART registers*/
#define USART_ISR_DEFAULT 6291664
#define USART_RDR_DEFAULT 0

/* Kiek laiko ieskot GPS kartais*//*60 reiskia 120s kas 2 sec*/
#define GPS_fix_time 120
#define GPS_FIRST_MESSAGE_WAIT 2000
#define GPS_FULL_MESSAGE_WAIT 1500

/*skambučio ilgis*/
#define CALL_LENGTH_SEC 30
#define SMS_READ_RETRIES 3

/*Profiliu numeriai*/
#define PROFILE_0	0
#define PROFILE_1 	1

/*Ka ijungti*/
//#define GNSS_MODE 0
//#define GSM_MODE 1
/*Debug message level*/
//#define EXTENDED DEBUG
/*Pagal ka parsinti zinute*/
#define m_SMS 0
#define m_DIAL_UP 1
#define m_ASK 2
#define m_OK_CHECK 3
#define m_SET 4
#define m_Read 5
#define m_flash_SMS 6

#define CR 13
#define LF 10
#define COMA ","

//#define DEVICE_PIN "0000"
#define GOOGLE_HTTP "https://maps.google.com/maps?q"

//typedef enum {
//	ASK = 0, SMS, DIAL_UP
//} AT_COMMAND_TYPEDEF;

typedef enum {
	DeviceProfile, SetDevice, Error
} AT_Msg_Type;

typedef struct {
	char repeat[30];
	char state[50];
	char posneg[10];
} AT_Ask_Data;

typedef struct {
	char value[30];
	char posneg[10];
} AT_Set_Data;

typedef struct {
	char number[20];
	char cmd[50];
	char input[50];
	char date[50];
	bool wrong_cmd;
} AT_Msg_content;

typedef struct {
	AT_Msg_content content;
	char data1[128];
	char data2[128];
	char data3[128];
} AT_Msg_Data;

typedef struct {
//	char rssi[2];
//	char precision[2];
	int8_t rssi;
	int8_t precision;
} AT_Network;

typedef struct {
	char data[100];
} AT_GPS_Data;

typedef enum {
	GNSS_MODE = 0, GSM_MODE = 1
} GNSS_GSM_Type;

typedef struct {
	AT_Ask_Data ask;
	AT_Set_Data set;
	AT_Msg_Data msg;
	AT_Network network_params;
	char answer[AT_ANSWER_SIZE];
	uint16_t answer_len;
	char input[128];
	GNSS_GSM_Type gsm_gnss;
	UART_HandleTypeDef hlpuart;
} AT_Struct_Data;

typedef struct {
	char raw[GNSS_RAW_SIZE];
	char name[30];
	char utc[9];
	float lat;
	char lat_dir;
	float lon;
	char lon_dir;
	uint8_t quality;
	uint8_t sats;
	float hdop;
	float alt;
	char a_units;
	float undulation;
	char u_units;
	uint8_t age;
	uint16_t stn_ID;
	uint8_t crc;
} GPGGA_Struct_data;

typedef struct {
	float lat;
	float lon;
} GOOGLE_Struct;

typedef struct {
	uint8_t lat_idx;
	uint8_t lon_idx;
} gpgga_helper;

typedef enum {
	DMA_USART = 0, STANDART_USART
} USART_TYPE;

typedef struct {
	uint16_t battery_mv;
	uint8_t battery_proc;
} MCU_bat_struct;

#endif /* LIBRARY_INC_DEF_MODULE_H_ */
