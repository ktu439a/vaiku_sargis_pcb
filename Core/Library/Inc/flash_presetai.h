/*
 * flash.h
 *
 *  Created on: Mar 2, 2020
 *      Author: elect
 *      flash_presetai failas yra pagrindinis kur laikomi presetai ir saugomi i flasha.
 *      kiti thread turi  savo local presetus kurie yra gaunami per queue.
 */

#ifndef FLASH_PRESETAI_H_
#define FLASH_PRESETAI_H_

#include "main.h"
#include "def_module.h"
#include "stdio.h"
/* Private typedef for FLASH--------------------------------------------------*/
// Sector 7 (pask sektorius:) 128 kB:
#define FLASH_PRESETAI_ADDR	0x0801F000
#define FLASH_USER_END_ADDR		0x0801F800

//#define FLASH_PRESETAI_ADDR	0x08030000
#define FLASH_SECTOR_NR		1
//#define FLASH_USER_END_ADDR		0x080FFFFF
#define WORD_LENGTH 64
// del bootloader palikt 32 baitus
typedef struct {
	char SOS_nr[8][20];
	char PIN[4];
} __attribute__((packed, aligned(4))) Control_struct;

/// cia definint visus naudojamus presetus:
typedef struct {
	uint32_t FlashOK_user;
	Control_struct par;

} __attribute__((packed, aligned(4))) Presetai_struct;

//Extern
extern Presetai_struct params;
// funkcijos flashui
void Read_from_flash(uint8_t *masyvas, uint16_t ilgis, uint32_t adresas);
uint8_t Write_to_flash(uint64_t *koef_array, uint16_t ilgis,
		uint32_t FlashAddress);
uint32_t Flash_Write_Data(uint32_t StartPageAddress, uint32_t *Data,
		uint16_t numberofwords);
void Get_presets_from_flash();
void Set_presets_to_flash();
void presets_2_structures(void);
void structures_2_presets(void);
uint8_t FLASH_WRITE(uint32_t destination, uint32_t *p_source, uint32_t length);
#endif /* FLASH_H_ */
