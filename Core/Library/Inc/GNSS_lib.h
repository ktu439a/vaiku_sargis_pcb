/*
 * GNSS_lib.h
 *
 *  Created on: 2021-07-22
 *      Author: Lituotojas
 */

#ifndef LIBRARY_INC_GNSS_LIB_H_
#define LIBRARY_INC_GNSS_LIB_H_

#include "usart.h"
#include "main.h"
#include "def_module.h"
#include "stdio.h"
#include "stm32l4xx_hal.h"
#include <string.h>

uint8_t GPS_Parser(GPGGA_Struct_data *GNSS, GOOGLE_Struct *GOOGLE);
uint8_t GPS_helper_f(char *token, float *data_f);
uint8_t GPS_helper_ch(char *token, char *data_ch);

uint8_t GPS_helper_u8(char *token, uint8_t *data_u8);

uint8_t GPS_helper_u16(char *token, uint16_t *data_u16);
uint8_t NoAnswer_GNSS(GPGGA_Struct_data *GNSS);
void NullTrimmerFront_GNSS(GPGGA_Struct_data *GNSS);
#endif /* LIBRARY_INC_GNSS_LIB_H_ */
