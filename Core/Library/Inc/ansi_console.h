/*
 * ansi_console.h
 *
 *  Created on: Feb 26, 2021
 *      Author: elect
 */

#ifndef INC_ANSI_CONSOLE_H_
#define INC_ANSI_CONSOLE_H_

#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

extern UART_HandleTypeDef huart1;
/*
 * -----------------------------------------------------------------------------
 * --- Define for function on/off -----------------------------------------------------------
 */

#define HAL_FEATURE_OFF                             0
#define HAL_FEATURE_ON                              1

#define HAL_DBG_TRACE                               HAL_FEATURE_ON
#define HAL_DBG_TRACE_COLOR                         HAL_FEATURE_ON
#define HAL_DBG_NON_BLOCKING						HAL_FEATURE_ON
#define HAL_USE_PRINTF_UART                        	&huart1
/*
 * -----------------------------------------------------------------------------
 * --- Macros for printing -----------------------------------------------------------
 */
void hal_mcu_trace_print(const char *fmt, ...);

#if( HAL_DBG_TRACE_COLOR == HAL_FEATURE_ON )
#define HAL_DBG_TRACE_COLOR_BLACK               "\x1B[0;30m"
#define HAL_DBG_TRACE_COLOR_RED                 "\x1B[0;31m"
#define HAL_DBG_TRACE_COLOR_GREEN               "\x1B[0;32m"
#define HAL_DBG_TRACE_COLOR_YELLOW              "\x1B[0;33m"
#define HAL_DBG_TRACE_COLOR_BLUE                "\x1B[0;34m"
#define HAL_DBG_TRACE_COLOR_MAGENTA             "\x1B[0;35m"
#define HAL_DBG_TRACE_COLOR_CYAN                "\x1B[0;36m"
#define HAL_DBG_TRACE_COLOR_WHITE               "\x1B[0;37m"
#define HAL_DBG_TRACE_COLOR_DEFAULT             "\x1B[0m"
#else
#define HAL_DBG_TRACE_COLOR_BLACK   ""
#define HAL_DBG_TRACE_COLOR_RED     ""
#define HAL_DBG_TRACE_COLOR_GREEN   ""
#define HAL_DBG_TRACE_COLOR_YELLOW  ""
#define HAL_DBG_TRACE_COLOR_BLUE    ""
#define HAL_DBG_TRACE_COLOR_MAGENTA ""
#define HAL_DBG_TRACE_COLOR_CYAN    ""
#define HAL_DBG_TRACE_COLOR_WHITE   ""
#define HAL_DBG_TRACE_COLOR_DEFAULT ""
#endif

#if ( HAL_DBG_TRACE ) && !defined (PERF_TEST_ENABLED)

#if ( UNIT_TEST_DBG )
        #define HAL_DBG_TRACE_PRINTF( ... )  printf (  __VA_ARGS__ )
    #else
#define HAL_DBG_TRACE_PRINTF( ... )  hal_mcu_trace_print (  __VA_ARGS__ )
#endif

#define HAL_DBG_TRACE_MSG( msg )                                           \
    do                                                                         \
    {                                                                          \
        HAL_DBG_TRACE_PRINTF( HAL_DBG_TRACE_COLOR_DEFAULT );                   \
        HAL_DBG_TRACE_PRINTF( msg );                                           \
    } while ( 0 );

#define HAL_DBG_TRACE_MSG_COLOR( msg, color )                              \
    do                                                                         \
    {                                                                          \
        HAL_DBG_TRACE_PRINTF( color );                                         \
        HAL_DBG_TRACE_PRINTF( msg );                                           \
        HAL_DBG_TRACE_PRINTF( HAL_DBG_TRACE_COLOR_DEFAULT );                   \
    } while ( 0 );

#define HAL_DBG_TRACE_INFO( ... )                                          \
    do                                                                         \
    {                                                                          \
        HAL_DBG_TRACE_PRINTF( HAL_DBG_TRACE_COLOR_GREEN );                     \
        HAL_DBG_TRACE_PRINTF( "INFO : " );                                     \
        HAL_DBG_TRACE_PRINTF( __VA_ARGS__ );                                   \
        HAL_DBG_TRACE_PRINTF( HAL_DBG_TRACE_COLOR_DEFAULT );                   \
    } while ( 0 );

#define HAL_DBG_TRACE_WARNING( ... )                                       \
    do                                                                         \
    {                                                                          \
        HAL_DBG_TRACE_PRINTF( HAL_DBG_TRACE_COLOR_YELLOW );                    \
        HAL_DBG_TRACE_PRINTF( "WARN : " );                                     \
        HAL_DBG_TRACE_PRINTF( __VA_ARGS__ );                                   \
        HAL_DBG_TRACE_PRINTF( HAL_DBG_TRACE_COLOR_DEFAULT );                   \
    } while ( 0 );

#define HAL_DBG_TRACE_ERROR( ... )                                         \
    do                                                                         \
    {                                                                          \
        HAL_DBG_TRACE_PRINTF( HAL_DBG_TRACE_COLOR_RED );                       \
        HAL_DBG_TRACE_PRINTF( "ERROR: " );                                     \
        HAL_DBG_TRACE_PRINTF( __VA_ARGS__ );                                   \
        HAL_DBG_TRACE_PRINTF( HAL_DBG_TRACE_COLOR_DEFAULT );                   \
    } while ( 0 );

#define HAL_DBG_TRACE_ARRAY( msg, array, len )                             \
    do                                                                         \
    {                                                                          \
        HAL_DBG_TRACE_PRINTF("%s - (%lu bytes):\n", msg, ( uint32_t )len );    \
        for( uint32_t i = 0; i < ( uint32_t )len; i++ )                        \
        {                                                                      \
            if( ( ( i % 16 ) == 0 ) && ( i > 0 ) )                             \
            {                                                                  \
                HAL_DBG_TRACE_PRINTF("\n");                                    \
            }                                                                  \
            HAL_DBG_TRACE_PRINTF( " %02X", array[i] );                         \
        }                                                                      \
        HAL_DBG_TRACE_PRINTF( "\n" );                                          \
    } while ( 0 );

#define HAL_DBG_TRACE_PACKARRAY( msg, array, len )   \
    do                                                   \
    {                                                    \
        for( uint32_t i = 0; i < ( uint32_t ) len; i++ ) \
        {                                                \
            HAL_DBG_TRACE_PRINTF( "%02X", array[i] );    \
        }                                                \
    } while( 0 );

#else
#define HAL_DBG_TRACE_PRINTF( ... )
#define HAL_DBG_TRACE_MSG( msg )
#define HAL_DBG_TRACE_MSG_COLOR( msg, color )
#define HAL_DBG_TRACE_INFO( ... )
#define HAL_DBG_TRACE_WARNING( ... )
#define HAL_DBG_TRACE_ERROR( ... )
#define HAL_DBG_TRACE_ARRAY( msg, array, len )
#define HAL_DBG_TRACE_PACKARRAY( ... )

#endif

#if defined (PERF_TEST_ENABLED)
    #define HAL_PERF_TEST_TRACE_PRINTF( ... )  hal_mcu_trace_print (  __VA_ARGS__ )
#else
#define HAL_PERF_TEST_TRACE_PRINTF( ... )
#endif

////////////// ************************ NON bocking define
void MergeString(char *msg);
void ClearString();
void TransmittString();
extern char Ansi_string[255];

#ifdef HAL_DBG_NON_BLOCKING
#define HAL_DBG_TRACE_DMA( msg, color )             \
    do                                              \
{                                                   \
    	ClearString();								\
		MergeString(color);							\
		MergeString(msg);							\
		TransmittString();							\
    } while ( 0 );
#endif

#endif /* INC_ANSI_CONSOLE_H_ */
