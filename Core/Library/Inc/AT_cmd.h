/*
 * AT_cmd.h
 *
 *  Created on: Jul 2, 2021
 *      Author: Lituotojas
 */

#ifndef LIBRARY_INC_AT_CMD_H_
#define LIBRARY_INC_AT_CMD_H_
#include "usart.h"
#include "main.h"
#include "def_module.h"
#include "stdio.h"
#include "stm32l4xx_hal.h"
#include <string.h>
#include "GNSS_lib.h"
#include "gpio.h"

#define SMS_WRITE_FLASH
//#define SMS_GPIO_INT

uint8_t CREG_parser(AT_Struct_Data *AT);
uint8_t AT_SendWaitParse_FlashSMS(AT_Struct_Data *AT);
uint8_t AT_SendWaitParse_FlashSMS_CMTI(AT_Struct_Data *AT);
uint8_t AT_SendWait(AT_Struct_Data *AT, char msg_tx[]);
uint8_t AT_Parser(AT_Struct_Data *AT, uint8_t AT_COMMAND_TYPE);
uint8_t GNSS_Init(AT_Struct_Data *AT);
uint8_t GSM_Init(AT_Struct_Data *AT);
void AT_WaitUnsoli(AT_Struct_Data *AT);
void NUMBERS_READ_SIM(Presetai_struct *params, AT_Struct_Data *AT);
void SMS_sent(Presetai_struct *params, AT_Struct_Data *AT, char text[],
		uint8_t text_length);
uint8_t GNSS_Init(AT_Struct_Data *AT);
uint8_t GNSS_ON(AT_Struct_Data *AT);
uint8_t GNSS_OFF(AT_Struct_Data *AT);

uint8_t SOS_CALL(Presetai_struct *params, AT_Struct_Data *AT);
uint8_t IsAscii(AT_Struct_Data *AT, uint8_t length);
uint8_t GSM_GNSS_REBOOT(AT_Struct_Data *AT);
void NullTrimmerFront(AT_Struct_Data *AT);
uint8_t SMS_ECHO_CMP(AT_Struct_Data *AT, char sms_str_c[]);
uint8_t NoAnswer(AT_Struct_Data *AT);
uint16_t AT_Battery_meas(AT_Struct_Data *AT);
uint8_t GNSS_WaitForRx(AT_Struct_Data *AT);
uint8_t LoadProfile(AT_Struct_Data *AT, uint8_t profile_nr);
uint8_t GNSS_GSM_SWITCH(AT_Struct_Data *AT);
void AT_SendWaitParseRepeat(AT_Struct_Data *AT, char msg_tx[]);
uint8_t CMGS_SEARCH(AT_Struct_Data *AT);
uint8_t SMS_Interpreter(Presetai_struct *params, AT_Struct_Data *AT,
		GPGGA_Struct_data *GNSS, GOOGLE_Struct *GOOGLE, char *sms_str_c);
uint8_t GNSS_COORDINATES(AT_Struct_Data *AT);
uint8_t GNSS_Interpreter(AT_Struct_Data *AT, GPGGA_Struct_data *GNSS,
		GOOGLE_Struct *GOOGLE);
uint8_t GNSS_Interpreter_all_night_long(AT_Struct_Data *AT,
		GPGGA_Struct_data *GNSS, GOOGLE_Struct *GOOGLE);
uint8_t GNSS_Interpreter_Humidity_overload(AT_Struct_Data *AT,
		GPGGA_Struct_data *GNSS, GOOGLE_Struct *GOOGLE);
void AT_SendWaitAskParams(AT_Struct_Data *AT, char msg_tx[]);
uint8_t CSQ_parser(AT_Struct_Data *AT);
uint8_t CALL_STATE(AT_Struct_Data *AT);
void CRLFTrimmerFront(AT_Struct_Data *AT);
void AT_SendWaitParseOKRepeat(AT_Struct_Data *AT, char msg_tx[]);
uint8_t AT_SendWaitSMS_flash(AT_Struct_Data *AT, char msg_tx[]);
void CUT_QUOTATION(char *nr);
uint8_t AT_change_baudrate(UART_HandleTypeDef *huart, AT_Struct_Data *AT);
void tellit_reset(AT_Struct_Data *AT);
uint8_t AT_SendWait_DEBUG(AT_Struct_Data *AT, char msg_tx[]);
#endif /* LIBRARY_INC_AT_CMD_H_ */
