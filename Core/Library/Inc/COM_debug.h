/*
 * COM_debug.H
 *
 *  Created on: Jul 19, 2021
 *      Author: Lituotojas
 */
/* Uzsidefininti uart koks naudojamas h faile */
#ifndef LIBRARY_INC_COM_DEBUG_H_
#define LIBRARY_INC_COM_DEBUG_H_
#include "usart.h"
#include "main.h"
#include "def_module.h"
#include "stdio.h"
#include "stm32l4xx_hal.h"
#include <string.h>

extern UART_HandleTypeDef huart1;
void printf_debug(uint8_t info[]);

#endif /* LIBRARY_INC_COM_DEBUG_H_ */
