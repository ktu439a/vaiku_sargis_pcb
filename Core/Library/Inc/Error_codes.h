/*
 * Error_codes.h
 *
 *  Created on: 2021-08-12
 *      Author: Augustinas
 */

#ifndef LIBRARY_INC_ERROR_CODES_H_
#define LIBRARY_INC_ERROR_CODES_H_

#define OK 0x01

#define NO_SIM_CARD_ERROR 0x10
#define NOT_ASCII_ERROR 0x11
#define NO_ANSWER_ERROR 0x12
#define TOKEN_NULL 0x13
#define GNSS_NO_ANSWER 0x20

#endif /* LIBRARY_INC_ERROR_CODES_H_ */
