/*
 * GNSS_lib.c
 *
 *  Created on: 2021-07-22
 *      Author: Lituotojas
 */

#include "GNSS_lib.h"

/*GNSS parseris*/
uint8_t GPS_Parser(GPGGA_Struct_data *GNSS, GOOGLE_Struct *GOOGLE) {

	uint16_t temp_lat;
	uint16_t temp_lon;

	static char temp[20];
	static uint8_t pos_idx = 0;
	pos_idx = 0;

	memset(temp, '0', sizeof(temp));

	if (NoAnswer_GNSS(GNSS) > 35) {
		return GNSS_NO_ANSWER;
	}

	NullTrimmerFront_GNSS(GNSS);

	char *token = strtok(GNSS->raw, ",");
	strcpy(GNSS->name, token);

//	HAL_DBG_TRACE_PRINTF("%s\n", token);

	if (GPS_helper_ch(token, GNSS->utc) == 1) {
		pos_idx++;
	}

	if (GPS_helper_f(token, &GNSS->lat) == 1) {
		pos_idx++;
	}

	if (GPS_helper_ch(token, &GNSS->lat_dir) == 1) {
		pos_idx++;
	}

	if (GPS_helper_f(token, &GNSS->lon) == 1) {
		pos_idx++;
	}

	if (GPS_helper_ch(token, &GNSS->lon_dir) == 1) {
		pos_idx++;
	}

	if (GPS_helper_u8(token, &GNSS->quality) == 1) {
		pos_idx++;
	}

	if (GPS_helper_u8(token, &GNSS->sats) == 1) {
		pos_idx++;
	}

	if (GPS_helper_f(token, &GNSS->hdop) == 1) {
		pos_idx++;
	}

	if (GPS_helper_f(token, &GNSS->alt) == 1) {
		pos_idx++;
	}

	if (GPS_helper_ch(token, &GNSS->a_units) == 1) {
		pos_idx++;
	}

	if (GPS_helper_f(token, &GNSS->undulation) == 1) {
		pos_idx++;
	}

	if (GPS_helper_ch(token, &GNSS->u_units) == 1) {
		pos_idx++;
	}

	if (GPS_helper_u8(token, &GNSS->age) == 1) {
		pos_idx++;
	}

	if (GPS_helper_u16(token, &GNSS->stn_ID) == 1) {
		pos_idx++;
	}

	if (GPS_helper_u8(token, &GNSS->crc) == 1) {
		pos_idx++;
	}

	if (pos_idx > 5) {

		temp_lat = (int) GNSS->lat / 100;
		temp_lon = (int) GNSS->lon / 100;

		if (GNSS->lat_dir == 'N') {
			GOOGLE->lat = temp_lat + ((float) GNSS->lat - temp_lat * 100) / 60;
		} else {
			GOOGLE->lat = temp_lat - ((float) GNSS->lat - temp_lat * 100) / 60;
		}

		if (GNSS->lon_dir == 'E') {
			GOOGLE->lon = temp_lon + ((float) GNSS->lon - temp_lon * 100) / 60;
		} else {
			GOOGLE->lon = temp_lon - ((float) GNSS->lon - temp_lon * 100) / 60;
		}
		return 1;
	} else {
		GOOGLE->lon = 0;
		GOOGLE->lat = 0;
		return 0;
	}

}

uint8_t GPS_helper_f(char *token, float *data_f) {
	static uint8_t temp[20];
	memset(temp, '0', sizeof(temp));

	token = strtok(NULL, COMA);
	if (token == NULL) {
		data_f = 0;
		return 0;
	}
	strcpy(temp, token);
	*data_f = atof(temp);
	return 1;
}

uint8_t GPS_helper_ch(char *token, char *data_ch) {
	static uint8_t temp[20];
	memset(temp, '0', sizeof(temp));

	token = strtok(NULL, COMA);
	if (token == NULL) {
		data_ch = '0';
		return 0;
	}
	strcpy(data_ch, token);
	return 1;
}

uint8_t GPS_helper_u8(char *token, uint8_t *data_u8) {
	static uint8_t temp[20];
	memset(temp, '0', sizeof(temp));

	token = strtok(NULL, COMA);
	if (token == NULL) {
		data_u8 = 0;
		return 0;
	}
	strcpy(temp, token);
	*data_u8 = atoi(temp);
	return 1;
}

uint8_t GPS_helper_u16(char *token, uint16_t *data_u16) {
	static uint8_t temp[20];
	memset(temp, '0', sizeof(temp));

	token = strtok(NULL, COMA);
	if (token == NULL) {
		data_u16 = 0;
		return 0;
	}
	strcpy(temp, token);
	*data_u16 = atoi(temp);
	return 1;
}

uint8_t NoAnswer_GNSS(GPGGA_Struct_data *GNSS) {
	static uint8_t noanswer_idx;
	noanswer_idx = 0;
	for (int i = 0; i < 40; i++) {
		if (GNSS->raw[i] == '\0') {
			noanswer_idx++;
		}
	}
	return noanswer_idx;
}

void NullTrimmerFront_GNSS(GPGGA_Struct_data *GNSS) {

	static char temp_matrix[255];
	static int i = 0;
	static uint8_t NULL_FOUND = 0;

	NULL_FOUND = 0;
	i = 0;
	memcpy(temp_matrix, GNSS->raw, 255);

	for (i = 0; i < 12; i++) {
		if (temp_matrix[i] == '\0' && temp_matrix[i + 1] != '\0') {
			NULL_FOUND = 1;
			break;
		}
		if (i == 10) {
			NULL_FOUND = 0;
			break;
		}
	}
	if (NULL_FOUND == 1) {
		memcpy(&GNSS->raw, &temp_matrix[i + 1], 100);
	}
}

