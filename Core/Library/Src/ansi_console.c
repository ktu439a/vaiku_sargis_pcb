/*
 * ansi_console.c
 *
 *  Created on: Feb 26, 2021
 *      Author: elect
 */
#include "ansi_console.h"
//------------------			Is Lora Semtech example kodas blokuojantis siuntimas.
static void vprint(const char *fmt, va_list argp);

void hal_mcu_trace_print(const char *fmt, ...) {
#if HAL_DBG_TRACE == HAL_FEATURE_ON
	va_list argp;
	va_start(argp, fmt);
	vprint(fmt, argp);
	va_end(argp);
#endif
}

#if( HAL_DBG_TRACE == HAL_FEATURE_ON )
static void vprint(const char *fmt, va_list argp) {
	char string[255];

	if (HAL_USE_PRINTF_UART != 0) {
		if (0 < vsprintf(string, fmt, argp))  // build string
				{
			HAL_UART_Transmit(HAL_USE_PRINTF_UART, (uint8_t*) string, strlen(string), 10);
		}
	}
}
#endif

//------------------			Naujos funkcijos norint padaryt neblokuojama siuntima:
// Tinka kai siunciama fiksuotos zinutes be jokiu parametru, naudojamas fiksuotas statinis masyvas. Pries dedant zinute reikia ji apnulinti.
// H7 svarbu kad masyvas butu D2 domaine, ir isjungti cache per MPU config.
char Ansi_string[255] __attribute__((section(".sram_sekcija")));

void MergeString(char *msg) {
	strcat(Ansi_string, msg);
}
void ClearString() {
	memset(Ansi_string, 0, sizeof(Ansi_string));
}
void TransmittString() {
	HAL_StatusTypeDef error = HAL_BUSY;
	while (error != HAL_OK) {
		error = HAL_UART_Transmit_DMA(HAL_USE_PRINTF_UART, (uint8_t*) Ansi_string, strlen(Ansi_string)); // paleidus siuntimas vyks fone.
	}
}
