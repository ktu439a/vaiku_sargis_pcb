/*
 * AT_cmd.c
 *
 *  Created on: Jul 2, 2021
 *      Author: Lituotojas
 */
#include "AT_cmd.h"

extern bool check_herkon;

void tellit_reset(AT_Struct_Data *AT);
uint8_t AT_Parser(AT_Struct_Data *AT, uint8_t AT_COMMAND_TYPE);
/*MODEM*/
//AT+IPR=115200 data rate
//GPIO_02 aukstas hurina kai skambinu
/*GSM*/

//ATH padeda ragelį
//AT+CPAS ar dabar skambina ir ar galima siusti komandas
//AT+CMGL skaito zinutes
//AT+CNMI=1,2,0,1,0 cia kai atsiuncia zinute kad iskart gauciau
//AT+CMGF=1 kad nustatyti jog text
//AT+CSMP=17,167,0,0 nusistatyti siuntimo parametrus
//AT+CMGD=1,4 istrina visas zinutes
//
/*GNSS*/

//AT$GPSCFG=0,0
//AT$GPSCFG=1,4321
//AT$GPSCFG=2,1
//AT$GPSCFG?
//AT#REBOOT
//AT$GPSCFG?
//AT$GPSP=1
/*GSM init inicializuot*/
uint8_t GSM_Init(AT_Struct_Data *AT) {
	/*Pasitikrinti kad daiktas veikia*/
//	AT_SendWaitParseRepeat(AT, "AT\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT\r\n");

	/*Pasitikrinti ar yra kortelė*/
	AT_SendWaitParseRepeat(AT, "AT+CPIN?\r\n");

	/*Pasitikrinti ar įjungtas GNSS/GPS*/
	//TODO: jeigu išjungtas tai įjungt
	AT_SendWaitParseRepeat(AT, "AT+CFUN?\r\n");

	/*Nustatyti gauti žemą kai gauna žinutę*/
//	AT_SendWaitParseRepeat(AT, "AT#E2SMSRI=400\r\n");
	/*Jog kai gaunu žinutę priimčiau siuncia kiaurai*/
#ifdef WRITE_THROUGH
	AT_SendWaitParseRepeat(AT, "AT+CNMI=1,2,0,1,0\r\n");
#elif defined(SMS_GPIO_INT)
	AT_SendWaitParseOKRepeat(AT, "AT#GPIO=2,0,10\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT#E2SMSRI=1000\r\n");
	AT_SendWaitParseRepeat(AT, "AT+CNMI=0,1,0,2,0\r\n");

#elif defined(SMS_WRITE_FLASH)
	AT_SendWaitParseOKRepeat(AT, "AT+CNMI=3,1,0,1,0\r\n");
//	AT_SendWaitParseRepeat(AT, "AT+CNMI=3,1,0,1,0\r\n");
#endif
	/*Jog kai gaunu žinutę priimčiau siuncia pasiuntus komandą*/
//	AT_SendWaitParseRepeat(AT, "AT+CNMI=3,2,0,1,0\r\n");
	/*Nustato text formatą*/
//	AT_SendWaitParseRepeat(AT, "AT+CMGF=1\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT+CMGF=1\r\n");

	/*Nusistatyti siuntimo parametrus*/
//	AT_SendWaitParseRepeat(AT, "AT+CSMP=17,167,0,0\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT+CSMP=17,167,0,0\r\n");
	/*Nustatome jog priimtume*/
//	AT_SendWaitParseRepeat(AT, "AT+CSMP=17,167,0,0\r\n");
	/* SMS Ring Indicator*/
//	AT_SendWaitParseRepeat(AT, "AT#W=1000\r\n");
	/*PSM*/
//	AT_SendWait(AT, "AT+CPSMS=1,\"01000011\",\"01000011\"\r\n");
//	if(AT_Parser(AT, m_OK_CHECK)!=1){
//		return 0;
//	}
	/*eDRX*/
//	AT_SendWait(AT, "AT+CEDRXS=1,2,\"1001\"\r\n");
//	if(AT_Parser(AT, m_OK_CHECK)!=1){
//		return 0;
//	}
	return 1;
}

void READ_CEREG(AT_Struct_Data *AT) {
	AT_SendWaitAskParams(AT, "AT+CEREG?\r\n");
}

uint8_t CMGS_CME_chech(char msg_state[]) {
	static char repeat[100];
	static char result[50];
	static char CRLF[] = "\r\n";

	memset(repeat, 0, 100);
	memset(result, 0, 50);

	char *token;
	token = strtok((char*) msg_state, CRLF);
	if (token == NULL) {
		return TOKEN_NULL;
	}
	strcpy(repeat, token);

	token = strtok(NULL, CRLF);
	if (token == NULL) {
		return TOKEN_NULL;
	}
	strcpy(result, token);
	if (strncmp(result, "+CMGS", 5) == 0) {
		return 1;
	} else if (strncmp(result, "+CME", 4) == 0) {
		return 0;
	}
	return 0;
}

/*Žinutės siuntimas*/
void SMS_sent(Presetai_struct *params, AT_Struct_Data *AT, char text[],
		uint8_t text_length) {
	static char prefix[50];
//	static char text_temp[255];
	static char arrow[100];
	static char msg_state[100];
	static uint8_t msg_return_state;

	msg_return_state = 0;
	memset(prefix, '\0', sizeof(prefix));

	sprintf(prefix, "AT+CMGS=%s,145\r\n", &params->par.SOS_nr[0][0]);
	HAL_UART_Transmit(&AT->hlpuart, (uint8_t*) prefix, strlen(prefix),
	AT_WAIT_TIME);
	HAL_UART_Receive(&AT->hlpuart, (uint8_t*) arrow, 100, 20 * BAUDRATE_COEFF);
	memset(prefix, '\0', sizeof(prefix));

	text[text_length] = '';

	HAL_UART_Transmit(&AT->hlpuart, (uint8_t*) text, text_length + 1,
	AT_WAIT_TIME);

}

/*GNSS inicializavimas*/
uint8_t GNSS_Init(AT_Struct_Data *AT) {
	/*0 profilis yra GPS PRIORITY
	 * 1 profilis yra WWAN PRIORITY*/
	/*Kad viskas nustatyta tik GPS priority*/
//	AT_SendWait(AT, "AT$GPSCFG=0,0\r\n");
//	if (AT_Parser(AT, m_OK_CHECK) != 1) {
//		return 0;
//	}
	/*Kad viskas nustatyta tik WWAN priority*/
//	AT_SendWait(AT, "AT$GPSCFG=0,1\r\n");
//	if (AT_Parser(AT, m_OK_CHECK) != 1) {
//		return 0;
//	}
	if (AT->gsm_gnss == GSM_MODE) {
		AT_SendWaitParseOKRepeat(AT, "AT$GPSCFG=0,1\r\n");
//		AT_SendWaitParseRepeat(AT, "AT$GPSCFG=0,1\r\n");
	} else if (AT->gsm_gnss == GNSS_MODE) {
		AT_SendWaitParseOKRepeat(AT, "AT$GPSCFG=0,0\r\n");
//		AT_SendWaitParseRepeat(AT, "AT$GPSCFG=0,0\r\n");
	}

//	AT_SendWaitParseRepeat(AT, "AT$GPSCFG=1,1\r\n");
//	AT_SendWaitParseRepeat(AT, "AT$GPSCFG=1,2\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT$GPSCFG=1,2\r\n");

//	AT_SendWaitParseRepeat(AT, "AT$GPSCFG=2,1\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT$GPSCFG=2,1\r\n");

	AT_SendWait(AT, "AT#REBOOT\r\n");
	HAL_Delay(1000);
	HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
	HAL_Delay(5000);

//	AT_SendWaitParseRepeat(AT, "AT\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT\r\n");

//	AT_SendWaitParseRepeat(AT, "AT$GPSP=1\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT$GPSP=1\r\n");

//	AT_SendWaitParseRepeat(AT, "AT$GPSNMUN=0,0,0,0,0,0,0\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT$GPSNMUN=0,0,0,0,0,0,0\r\n");

	return 1;
}

uint8_t GNSS_COORDINATES(AT_Struct_Data *AT) {
//	AT_SendWaitParseRepeat(AT, "AT$GPSNMUN=1,1,0,0,0,0,0\r\n");
	AT_SendWaitParseOKRepeat(AT, "AT$GPSNMUN=1,1,0,0,0,0,0\r\n");
}

/*GNSS GSM switch pakeisti prioritetą, ką siųst*/
uint8_t GNSS_GSM_SWITCH(AT_Struct_Data *AT) {
//	LoadProfile(AT, PROFILE_0);
	if (AT->gsm_gnss == GNSS_MODE) {
//		AT_SendWaitParseRepeat(AT, "AT$GPSCFG=0,0\r\n");
		AT_SendWaitParseOKRepeat(AT, "AT$GPSCFG=0,0\r\n");

		AT_SendWait(AT, "AT#REBOOT\r\n");

		HAL_Delay(1000);
		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
		HAL_Delay(5000);
//		AT_SendWaitParseRepeat(AT, "AT$GPSP=1\r\n");
		AT_SendWaitParseOKRepeat(AT, "AT$GPSP=1\r\n");
//		AT_SendWaitParseRepeat(AT, "AT$GPSNMUN=1,1,0,0,0,0,0\r\n");
		AT_SendWaitParseOKRepeat(AT, "AT$GPSNMUN=1,1,0,0,0,0,0\r\n");
	} else if (AT->gsm_gnss == GSM_MODE) {

		AT->gsm_gnss = GSM_MODE;

		if (GNSS_Init(AT) != 1) {
			HAL_DBG_TRACE_MSG_COLOR("GNSS KLAIDA\r\n", HAL_DBG_TRACE_COLOR_RED);
			while (1) {
			}
		}

		if (GSM_Init(AT) != 1) {
			HAL_DBG_TRACE_MSG_COLOR("GSM KLAIDA\r\n", HAL_DBG_TRACE_COLOR_RED);
			while (1) {
			}
		}
	} else {

		return 0;
	}

	return 1;
}

uint8_t GNSS_GSM_SWITCH_old(AT_Struct_Data *AT) {
//	LoadProfile(AT, PROFILE_0);
	if (AT->gsm_gnss == GNSS_MODE) {
		AT_SendWait(AT, "AT$GPSCFG=0,0\r\n");

	} else if (AT->gsm_gnss == GSM_MODE) {
//		AT_SendWait(AT, "AT$GPSNMUN=0,0,0,0,0,0,0\r\n");
//		AT_SendWait(AT, "AT$GPSCFG=0,1\r\n");

		AT_SendWaitParseOKRepeat(AT, "AT$GPSNMUN=0,0,0,0,0,0,0\r\n");
		AT_SendWaitParseOKRepeat(AT, "AT$GPSCFG=0,1\r\n");
	} else {

		return 0;
	}

	AT_SendWait(AT, "AT#REBOOT\r\n");
//	if (AT_Parser(AT, m_OK_CHECK) != 1) {
//		return 0;
//	}

	HAL_Delay(1000);
	HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
	HAL_Delay(10000);

	if (AT->gsm_gnss == GNSS_MODE) {
		AT_SendWaitParseOKRepeat(AT, "AT$GPSP=1\r\n");
//		AT_SendWait(AT, "AT$GPSP=1\r\n");
//		if (AT_Parser(AT, m_OK_CHECK) != 1) {
//			return 0;
//		}

//		AT_SendWait(AT, "AT$GPSNMUN=1,1,0,0,0,0,0\r\n");
		AT_SendWaitParseOKRepeat(AT, "AT$GPSNMUN=1,1,0,0,0,0,0\r\n");
//		if (AT_Parser(AT, m_SET) != 1) {
//			return 0;
//		}
		return 1;
	} else if (AT->gsm_gnss == GSM_MODE) {
		AT_SendWaitParseOKRepeat(AT, "AT$GPSNMUN=0,0,0,0,0,0,0\r\n");
//		AT_SendWait(AT, "AT$GPSNMUN=0,0,0,0,0,0,0\r\n");

		/*Jog kai gaunu žinutę priimčiau*/
#if defined(SMS_WRITE_FLASH)
//		AT_SendWaitParseRepeat(AT, "AT+CNMI=3,1,0,1,0\r\n");
		AT_SendWaitParseOKRepeat(AT, "AT+CNMI=3,1,0,1,0\r\n");
#elif defined(WRITE_THROUGH)
		AT_SendWait(AT, "AT+CNMI=1,2,0,1,0\r\n");
		#endif

		if (AT_Parser(AT, m_SET) != 1) {
			return 0;
		}
		/*Nustato text formatą*/
//		AT_SendWait(AT, "AT+CMGF=1\r\n");
//		if (AT_Parser(AT, m_SET) != 1) {
//			return 0;
//		}
		AT_SendWaitParseOKRepeat(AT, "AT+CMGF=1\r\n");
		/*Nusistatyti siuntimo parametrus*/
//		AT_SendWait(AT, "AT+CSMP=17,167,0,0\r\n");
//		if (AT_Parser(AT, m_SET) != 1) {
//			return 0;
//		}
		AT_SendWaitParseOKRepeat(AT, "AT+CSMP=17,167,0,0\r\n");

		return 1;
	} else {

		return 0;
	}

	return 1;
}

uint8_t GNSS_WaitForRx(AT_Struct_Data *AT) {
	HAL_UART_Receive(&AT->hlpuart, (uint8_t*) AT->answer, 100,
			20 * BAUDRATE_COEFF);
	return 1;
}

void AT_WaitUnsoli(AT_Struct_Data *AT) {
	HAL_UART_Receive_DMA(&AT->hlpuart, (uint8_t*) AT->answer, 6);
}
/*AT komandų siuntimui ir laukimui*/
uint8_t AT_SendWait(AT_Struct_Data *AT, char msg_tx[]) {
	/*Blocking AT cmd*/
	HAL_DBG_TRACE_PRINTF("%s\n", msg_tx);
	HAL_UART_Transmit(&AT->hlpuart, (uint8_t*) msg_tx, strlen(msg_tx),
			50 * BAUDRATE_COEFF);
	HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
	HAL_UART_Receive(&AT->hlpuart, (uint8_t*) AT->answer, 40,
			100 * BAUDRATE_COEFF);
	AT->answer_len = AT->hlpuart.RxXferCount;
	HAL_Delay(100);
	return 1;
}

uint8_t AT_SendWait_DEBUG(AT_Struct_Data *AT, char msg_tx[]) {
	/*Blocking AT cmd*/
	HAL_DBG_TRACE_PRINTF("%s\n", msg_tx);
	HAL_UART_Transmit(&AT->hlpuart, (uint8_t*) msg_tx, strlen(msg_tx),
			50 * BAUDRATE_COEFF);
	HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
	HAL_UART_Receive(&AT->hlpuart, (uint8_t*) AT->answer, AT_ANSWER_SIZE,
			100 * BAUDRATE_COEFF);
	AT->answer_len = AT->hlpuart.RxXferCount;
	HAL_Delay(100);
	return 1;
}

uint8_t AT_SendWaitSMS_flash(AT_Struct_Data *AT, char msg_tx[]) {
	/*Blocking AT cmd*/

	HAL_UART_Transmit(&AT->hlpuart, (uint8_t*) msg_tx, strlen(msg_tx),
			50 * BAUDRATE_COEFF);
	HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
	HAL_UART_Receive(&AT->hlpuart, (uint8_t*) AT->answer, 100,
			50 * BAUDRATE_COEFF);
	AT->answer_len = AT->hlpuart.RxXferCount;
	return 1;
}

uint8_t AT_Send(UART_HandleTypeDef huart1, char msg_tx[]) {

	HAL_UART_Transmit_DMA(&huart1, (uint8_t*) msg_tx, strlen(msg_tx));

	return 1;
}
/*AT komandų siuntimas ir klausinėjimas ar gerai? o dabar? o dabar?*/
void AT_SendWaitParseRepeat(AT_Struct_Data *AT, char msg_tx[]) {
	static uint8_t this_is_fine;
	static uint8_t err_idx = 0;

	err_idx = 0;
	this_is_fine = 0;
	while (this_is_fine == 0) {
		AT_SendWait(AT, msg_tx);
		if (AT_Parser(AT, m_SET) != 1) {
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
			this_is_fine = 0;
			err_idx++;
		} else {
			this_is_fine = 1;
		}
		if (err_idx == 10) {
			err_idx = 0;
			tellit_reset(AT);
		}
	}

}

uint8_t CMTI_search(AT_Struct_Data *AT) {

	for (int i = 0; i < 7; i++) {
		if (strncmp(&AT->answer[i], "CMT", 3) == 0) {
			return 1;
		}
	}

	return 0;
}
/*Flash žinučių nusiskaitymas*/
uint8_t AT_SendWaitParse_FlashSMS_CMTI(AT_Struct_Data *AT) {
	static uint8_t this_is_fine;
	static uint8_t failed_attemt_idx = 0;
	if (CMTI_search(AT) == 1) {
		failed_attemt_idx = 0;
		this_is_fine = 0;
		memset(AT->answer, '\0', sizeof(AT->answer));
		while (this_is_fine == 0) {
			AT_SendWaitSMS_flash(AT, "AT+CMGR=1\r\n");
			if (AT_Parser(AT, m_flash_SMS) != 1) {
				HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
				this_is_fine = 0;
				failed_attemt_idx++;
			} else {
				this_is_fine = 1;
			}
			if (failed_attemt_idx == SMS_READ_RETRIES) {
				AT_SendWait(AT, "AT+CMGD=1,4\r\n");
				if (AT_Parser(AT, m_OK_CHECK) != 1) {
					HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
				}
				return 0;
			}
		}

		this_is_fine = 0;
		failed_attemt_idx = 0;

		while (this_is_fine == 0) {
			AT_SendWait(AT, "AT+CMGD=1,4\r\n");
			if (AT_Parser(AT, m_OK_CHECK) != 1) {
				HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
				this_is_fine = 0;
				failed_attemt_idx++;
			} else {
				this_is_fine = 1;
			}
			if (failed_attemt_idx == SMS_READ_RETRIES) {
				return 0;
			}
		}
		return 1;
	}
	memset(AT->answer, '\0', sizeof(AT->answer));
	return 0;

}
/*Flash žinučių nusiskaitymas*/
uint8_t AT_SendWaitParse_FlashSMS(AT_Struct_Data *AT) {
	static uint8_t this_is_fine;
	static uint8_t failed_attemt_idx = 0;

	failed_attemt_idx = 0;
	this_is_fine = 0;
	memset(AT->answer, '\0', sizeof(AT->answer));
	while (this_is_fine == 0) {
		AT_SendWaitSMS_flash(AT, "AT+CMGR=1\r\n");
		if (AT_Parser(AT, m_flash_SMS) != 1) {
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
			this_is_fine = 0;
			failed_attemt_idx++;
		} else {
			this_is_fine = 1;
		}
		if (failed_attemt_idx == 10) {
			AT_SendWait(AT, "AT+CMGD=1,4\r\n");
			if (AT_Parser(AT, m_OK_CHECK) != 1) {
				HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
			}
			return 0;
		}
	}

	this_is_fine = 0;
	failed_attemt_idx = 0;

	while (this_is_fine == 0) {
		AT_SendWait(AT, "AT+CMGD=1,4\r\n");
		if (AT_Parser(AT, m_OK_CHECK) != 1) {
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
			this_is_fine = 0;
			failed_attemt_idx++;
		} else {
			this_is_fine = 1;
		}
		if (failed_attemt_idx == 10) {
			return 0;
		}
	}
	return 1;
	memset(AT->answer, '\0', sizeof(AT->answer));
	return 0;

}
/*Parametrų užklausimui , čia yra mano draugai CSQ ir CREG tikrint ar yra ryšys, jeigu nėra perkraunam modulį ir pabandom iš naujo */
void AT_SendWaitAskParams(AT_Struct_Data *AT, char msg_tx[]) {
	static uint8_t debug_led_blink = 0;
	debug_led_blink = 0;

	static uint8_t this_is_fine;
	this_is_fine = 0;
	static uint8_t creg_error = 0;
	static uint8_t csq_error = 0;

	creg_error = 0;
	csq_error = 0;
	while (this_is_fine == 0) {
		AT_SendWait(AT, msg_tx);
		if (AT_Parser(AT, m_ASK) != 1) {
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
			this_is_fine = 0;
		} else {
			if (strncmp(msg_tx, "AT+CSQ\r\n", 10) == 0) {
				this_is_fine = CSQ_parser(AT);
				if (this_is_fine == 0) {

					while (debug_led_blink < 10) {
						HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
								GPIO_PIN_SET);
						HAL_Delay(50);
						HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
								GPIO_PIN_RESET);
						HAL_Delay(50);
						debug_led_blink++;
					}
					csq_error++;
					debug_led_blink = 0;
				}
				if (csq_error == 20) {
					csq_error = 0;
					tellit_reset(AT);
				}
				HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
						GPIO_PIN_RESET);
			} else if (strncmp(msg_tx, "AT+CREG?\r\n", 10) == 0) {
				this_is_fine = CREG_parser(AT);
				if (this_is_fine == 0) {
					while (debug_led_blink < 10) {
						HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
								GPIO_PIN_SET);
						HAL_Delay(100);
						HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
								GPIO_PIN_RESET);
						HAL_Delay(100);
						debug_led_blink++;
					}
					debug_led_blink = 0;
					creg_error++;
				}

				HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
						GPIO_PIN_RESET);
				if (creg_error == 30) {
					creg_error = 0;
					tellit_reset(AT);
				}
			}
		}
		HAL_Delay(100);
	}
}

uint8_t CSQ_parser(AT_Struct_Data *AT) {
	static char temp[5];
	static char rssi[2];
	static char precision[2];
	char *token;
	for (int i = 0; i < 5; i++) {
		temp[i] = AT->ask.state[6 + i];
	}
//	memcpy(temp, AT->ask.state[6], 5);

	token = strtok(temp, ",");
	strcpy(rssi, token);
	token = strtok(NULL, ",");
	strcpy(precision, token);
	if (rssi[0] == '9' && rssi[1] == '9') {
		return 0;
	}

	AT->network_params.rssi = -113 + 2 * atoi(rssi);
	AT->network_params.precision = atoi(precision);
	return 1;
}

uint8_t CREG_parser(AT_Struct_Data *AT) {
	static char CRLF[] = "\r\n";
	static char temp[10];
	static char temp1[10];

	char *token;

	token = strtok(AT->answer, CRLF);
	strcpy(AT->ask.repeat, token);

//	token = strtok(NULL, CRLF);
//	strcpy(temp, token);
//
//	token = strtok(NULL, ' ');
//	strcpy(temp1, token);
//
//	token = strtok(temp, ' ');
//	strcpy(AT->ask.state, token);

//	if (AT->ask.state[10] != 1) {
//		return 0;
//	}
	if (AT->ask.state[9] == '1' || AT->ask.state[9] == '5') {
		return 1;
	}
	return 0;
}

void CUT_QUOTATION(char *nr) {
	static uint8_t start_idx = 0;
	static uint8_t nr_len = 1;
	static char temp[20];
	start_idx = 0;
	nr_len = 1;
	memcpy(temp, nr, 20);

	for (start_idx = 0; start_idx < 20; start_idx++) {
		if (*(nr + start_idx) == '\"') {
			while ((*(nr + nr_len) != '\"')) {
				nr_len++;
			}
			break;
		}
	}
	memset(nr, '\0', 20);
	memcpy(nr, &temp[start_idx + 1], nr_len - 1);
}
/*Žinutės parseris*/
uint8_t AT_Parser(AT_Struct_Data *AT, uint8_t AT_COMMAND_TYPE) {
	static char CRLF[] = "\r\n";
	static char SLASH[] = "\"";
	static char EQUAL[] = "=";
	char *token;
	/*patikrina ar ascii*/
	if (IsAscii(AT, 5) != 1) {
		HAL_DBG_TRACE_MSG_COLOR("NOT ASCIIr\n", HAL_DBG_TRACE_COLOR_GREEN)
		return NOT_ASCII_ERROR;
	}
	/*patikrina ar yra žinutėje kažkas*/
	if (NoAnswer(AT) > 35) {
		HAL_DBG_TRACE_MSG_COLOR("NO ANSWER missing characters \r\n",
				HAL_DBG_TRACE_COLOR_GREEN)
		HAL_DBG_TRACE_PRINTF("%s\n", AT->answer);
		HAL_DBG_TRACE_MSG_COLOR("\r\n", HAL_DBG_TRACE_COLOR_GREEN)
		return NO_ANSWER_ERROR;
	}

	/*Ieškot ar tikrai ča žinutės framas*/
	if (CMGS_SEARCH(AT) == 1) {
		HAL_DBG_TRACE_MSG_COLOR("CMGS rado /PDU zinute\r\n",
				HAL_DBG_TRACE_COLOR_GREEN)
		return 0;
	}
	/*Ištrint Null, jeigu pradžioje yra*/
	NullTrimmerFront(AT);
	/*Tikrina koks žinutės tipas*/
	switch (AT_COMMAND_TYPE) {
	/*klausia parametrų ir juos išparsina apačioje*/
	case m_ASK:
		memset(&AT->ask, '\0', sizeof(AT->ask));
		token = strtok((char*) AT->answer, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->ask.repeat, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->ask.state, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->ask.posneg, token);

		if (strncmp(AT->ask.posneg, "OK", 2) == 0) {
			HAL_DBG_TRACE_MSG_COLOR("GOOD\r\n", HAL_DBG_TRACE_COLOR_GREEN)
		} else {
			HAL_DBG_TRACE_PRINTF("Neatsako: %s\n", AT->ask.repeat);
			return 0;
		}

		HAL_DBG_TRACE_PRINTF(" %s\n", AT->ask.repeat);
		HAL_DBG_TRACE_PRINTF(" %s\n", AT->ask.state);
		HAL_DBG_TRACE_PRINTF(" %s\n", AT->ask.posneg);

		break;
		/*tikrina ar OK yra*/
	case m_OK_CHECK:
		memset(&AT->ask, '\0', sizeof(AT->ask));
		token = strtok((char*) AT->answer, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->ask.repeat, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->ask.posneg, token);

		if (strncmp(AT->ask.posneg, "OK", 2) == 0) {
			HAL_DBG_TRACE_MSG_COLOR("GOOD\r\n", HAL_DBG_TRACE_COLOR_GREEN)
			memset(AT->answer, '\0', sizeof(AT->answer));
			return 1;
		} else {
			HAL_DBG_TRACE_PRINTF("Neatsako: %s\n", AT->ask.repeat);
			memset(AT->answer, '\0', sizeof(AT->answer));
			return 0;
		}

		HAL_DBG_TRACE_PRINTF(" %s\n", AT->ask.repeat);
		HAL_DBG_TRACE_PRINTF(" %s\n", AT->ask.posneg);
		break;

		/*tikrinam ar pasetino gerai*/
	case m_SET:
		memset(&AT->set, '\0', sizeof(AT->ask));
		token = strtok((char*) AT->answer, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->set.value, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->set.posneg, token);

		if (strncmp(AT->set.posneg, "+CPIN: READY", strlen("+CPIN: READY")) == 0
				|| strncmp(AT->set.posneg, "+CFUN: 1", strlen("+CFUN: 1"))
						== 0) {
			HAL_DBG_TRACE_MSG_COLOR("GOOD\r\n", HAL_DBG_TRACE_COLOR_GREEN)
			memset(AT->answer, '\0', sizeof(AT->answer));
			return 1;
		}

		if (strncmp(AT->set.posneg, "OK", 2) == 0) {
			HAL_DBG_TRACE_MSG_COLOR("GOOD\r\n", HAL_DBG_TRACE_COLOR_GREEN)
			memset(AT->answer, '\0', sizeof(AT->answer));
			return 1;
		} else {
			HAL_DBG_TRACE_MSG_COLOR("AT neatsako\r\n", HAL_DBG_TRACE_COLOR_RED)
			memset(AT->answer, '\0', sizeof(AT->answer));
			return 0;
		}

		HAL_DBG_TRACE_PRINTF(" %s\n", AT->set.value);
		HAL_DBG_TRACE_PRINTF(" %s\n", AT->set.posneg);
		break;
		/*žinutės iš flash nuskaitymas išparsinimas*/
	case m_flash_SMS:

		AT->msg.content.wrong_cmd = false;
		memset(&AT->set, '\0', sizeof(AT->msg.content));
		/*SMS iskirti i dvi dalis data1 nr, laikas ir data2 info*/
		token = strtok((char*) AT->answer, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.data1, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.data2, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.data3, token);

		/*Iskirti info i komanda ir komandos verte*/
		token = strtok((char*) AT->msg.data3, EQUAL);
		if (token == NULL) {
//			return TOKEN_NULL;
			AT->msg.content.wrong_cmd = true;
		}
		if (AT->msg.content.wrong_cmd == false) {
			strcpy(AT->msg.content.cmd, token);
		}
		token = strtok(NULL, EQUAL);
		if (token == NULL) {
//			return TOKEN_NULL;
			AT->msg.content.wrong_cmd = true;
		}
		if (AT->msg.content.wrong_cmd == false) {
			strcpy(AT->msg.content.input, token);
		}
		/*Iskirti nr ir laika kada siusta*/
		token = strtok((char*) AT->msg.data2, COMA);
		if (token == NULL) {
			return TOKEN_NULL;
		}

		token = strtok(NULL, COMA);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.content.number, token);
		token = strtok(NULL, COMA);
		if (token == NULL) {
			return TOKEN_NULL;
		}

		token = strtok(NULL, COMA);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		token = strtok(NULL, COMA);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.content.date, token);

		CUT_QUOTATION(AT->msg.content.number);

		HAL_DBG_TRACE_PRINTF(" %s\n", AT->msg.content.number);
		HAL_DBG_TRACE_PRINTF(" %s\n", AT->msg.content.date);
		HAL_DBG_TRACE_PRINTF(" %s=%s\n", AT->msg.content.cmd,
				AT->msg.content.input);
		break;
		/*kitas žinutės parsinimas kai visa žinutė pareina, o ne iš uart*/
	case m_SMS:
		CRLFTrimmerFront(AT);
		memset(&AT->set, '\0', sizeof(AT->msg.content));
		/*SMS iskirti i dvi dalis data1 nr, laikas ir data2 info*/
		token = strtok((char*) AT->answer, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.data1, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.data2, token);

		/*Iskirti info i komanda ir komandos verte*/
		token = strtok((char*) AT->msg.data2, EQUAL);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.content.cmd, token);

//		while (token != NULL) {
//			token = strtok(NULL, EQUAL);
//			break;
//		}
//		token = strtok(NULL, EQUAL);
//
//		token = strtok(NULL, EQUAL);

		token = strtok(NULL, EQUAL);
		if (token == NULL) {
			return TOKEN_NULL;
		}

		strcpy(AT->msg.content.input, token);
		/*Iskirti nr ir laika kada siusta*/
		token = strtok((char*) AT->msg.data1, SLASH);
		if (token == NULL) {
			return TOKEN_NULL;
		}

		token = strtok(NULL, SLASH);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.content.number, token);
		token = strtok(NULL, SLASH);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		token = strtok(NULL, SLASH);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		token = strtok(NULL, SLASH);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->msg.content.date, token);

		HAL_DBG_TRACE_PRINTF(" %s\n", AT->msg.content.number);
		HAL_DBG_TRACE_PRINTF(" %s\n", AT->msg.content.date);
		HAL_DBG_TRACE_PRINTF(" %s=%s\n", AT->msg.content.cmd,
				AT->msg.content.input);
		break;

	case m_DIAL_UP:

		HAL_DBG_TRACE_MSG_COLOR("Skambina\r\n", HAL_DBG_TRACE_COLOR_GREEN)
		;
		break;
		/*skambučio stats tikrinimui*/
	case m_Read:

		token = strtok((char*) AT->answer, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->ask.repeat, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->ask.state, token);

		token = strtok(NULL, CRLF);
		if (token == NULL) {
			return TOKEN_NULL;
		}
		strcpy(AT->ask.posneg, token);

		if (strncmp(AT->ask.posneg, "0", 2) == 0) {
			HAL_DBG_TRACE_MSG_COLOR("Baigėsi\r\n", HAL_DBG_TRACE_COLOR_GREEN)
		} else if (strncmp(AT->ask.posneg, "4", 2) == 0) {
			HAL_DBG_TRACE_MSG_COLOR("Atsiliepė\r\n", HAL_DBG_TRACE_COLOR_GREEN)
		} else {
			HAL_DBG_TRACE_PRINTF("Vis dar vyksta: %s\n", AT->ask.repeat);
		}

		memset(AT->answer, '\0', sizeof(AT->answer));
		if (AT->ask.state[7] == '0') {
			return 1;
		} else if (AT->ask.state[7] == '4') {
			return 2;
		} else {
			return 0;
		}

		HAL_DBG_TRACE_PRINTF(" %s\n", AT->ask.repeat);
		HAL_DBG_TRACE_PRINTF(" %s\n", AT->ask.posneg);
		break;

	default:

		HAL_DBG_TRACE_MSG_COLOR("cmd not found\r\n", HAL_DBG_TRACE_COLOR_RED)
		;
		break;
	}

//	memset(AT->answer, '1', sizeof(AT->answer));
	memset(AT->answer, '\0', sizeof(AT->answer));
	return 1;
}

/*Kodas administritariui*/
/*Aktyvuot ir deaktyvuot*/
/*GPS ar GSM mode*/
/*Skambina telitas numeriui*/
/*Čia buvo galvota, jog daugiau numerių liko tik vienas*/
void NUMBERS_READ_SIM(Presetai_struct *params, AT_Struct_Data *AT) {
	static char CRLF[] = "\r\n";
	static char temp_data[30];
	AT_SendWait(AT, "AT&N1\r\n");

// Returns first token
	char *token = strtok((char*) AT->answer, CRLF);
	token = strtok(NULL, CRLF);
	strcpy(temp_data, token);
	HAL_DBG_TRACE_PRINTF("%s\n", token);

	token = strtok(temp_data, ";");
	token = strtok(NULL, ";");
	if (token != NULL) {
		strcpy(&params->par.SOS_nr[0][0], token);
		HAL_DBG_TRACE_PRINTF("%s\n", token);
	} else {
		HAL_DBG_TRACE_MSG_COLOR("Nera admin numerio\r\n",
				HAL_DBG_TRACE_COLOR_RED);
	}

}
/*Sos skambutis*/
uint8_t SOS_CALL(Presetai_struct *params, AT_Struct_Data *AT) {
	static char temp_str[50];
	memset(temp_str, '\0', sizeof(temp_str));
	sprintf(temp_str, "ATD%s;\r\n", params->par.SOS_nr[0]);
	AT_SendWait(AT, temp_str);
	if (AT_Parser(AT, m_OK_CHECK) != 1) {
		return 0;
	}
	return 1;
}
/*Kol negaunu and its been OK the band was all together tol klausinėju o dabar? o dabar? */
void AT_SendWaitParseOKRepeat(AT_Struct_Data *AT, char msg_tx[]) {
	static uint8_t this_is_fine;
	static uint8_t err_code = 0;
	this_is_fine = 0;
	err_code = 0;

	while (this_is_fine == 0) {
		AT_SendWait(AT, msg_tx);
		if (AT_Parser(AT, m_OK_CHECK) != 1) {
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
			this_is_fine = 0;
			err_code++;
		} else {
			this_is_fine = 1;
		}

		if (err_code == 10) {
			err_code = 0;
			tellit_reset(AT);
		}

	}

}
/*Tikrinu skambučio būseną, kad žinot ar atmetė ir tada ieškot gnss*/
uint8_t CALL_STATE(AT_Struct_Data *AT) {
	static uint8_t time_idx = 0;
	static uint8_t status_call = 0;
	time_idx = 0;

	while (1) {

		HAL_Delay(1000);
		AT_SendWait(AT, "AT+CPAS\r\n");
		time_idx++;
		status_call = AT_Parser(AT, m_Read);
		if (status_call == 1) {
			return 1;
		}
		if (time_idx == CALL_LENGTH_SEC) {
			if (status_call == 2) {
				AT_SendWaitParseRepeat(AT, "ATH\r\n");
			}
			break;
		}
	}
	return 1;
}

uint8_t IsAscii(AT_Struct_Data *AT, uint8_t length) {
	static int i = 0;
	i = 0;

	for (i = 0; i < length; i++) {
		if (AT->answer[i] > 127) {
			return 0;
		}
	}
	return 1;
}

void NullTrimmerFront(AT_Struct_Data *AT) {
	static char temp_matrix[255];
	static int i = 0;
	static uint8_t NULL_FOUND = 0;

	NULL_FOUND = 0;
	i = 0;
	memcpy(temp_matrix, AT->answer, 255);

	for (i = 0; i < 12; i++) {
		if (temp_matrix[i] == '\0' && temp_matrix[i + 1] != '\0') {
			NULL_FOUND = 1;
			break;
		}
		if (i == 10) {
			NULL_FOUND = 0;
			break;
		}
	}
	if (NULL_FOUND == 1) {
		memcpy(&AT->answer, &temp_matrix[i + 1], 100);
	}
}

void CRLFTrimmerFront(AT_Struct_Data *AT) {
	static char temp_matrix[255];
	static int i = 0;
	static uint8_t NULL_FOUND = 0;

	NULL_FOUND = 0;
	i = 0;
	memcpy(temp_matrix, AT->answer, 255);

	for (i = 0; i < 12; i++) {
		if (strncmp(&temp_matrix[i], "+CMT", 4) == 0) {
			NULL_FOUND = 1;
			break;
		}
		if (i == 10) {
			NULL_FOUND = 0;
			break;
		}
	}
	if (NULL_FOUND == 1) {
		memcpy(&AT->answer[2], &temp_matrix[i], 100);
		AT->answer[0] = '\r';
		AT->answer[1] = '\n';
	}
}
/*REBOOT*/
uint8_t GSM_GNSS_REBOOT(AT_Struct_Data *AT) {
//	static uint8_t device_state_on = 0;
//	device_state_on = 0;

//	AT_SendWait(AT, "AT\r\n");
//
//	if (AT_Parser(AT, m_OK_CHECK) != 1) {
//		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
//		HAL_Delay(5000);
//		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_RESET);
//		HAL_Delay(5000);
//		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
//		HAL_Delay(2000);
//		device_state_on = 1;
//	}
//	memset(AT->answer, '\0', 100);
//	AT_SendWait(AT, "AT\r\n");
//
//	AT_SendWaitParseRepeat(AT, "AT$GPSNMUN=0,0,0,0,0,0,0\r\n");

	AT_SendWait(AT, "AT#REBOOT\r\n");
	HAL_Delay(1000);

	HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_RESET);
	HAL_Delay(5000);
	HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
	HAL_Delay(2000);

	AT_SendWaitParseOKRepeat(AT, "AT\r\n");

//	AT_SendWait(AT, "AT\r\n");
//	AT_SendWait(AT, "AT\r\n");
}

//uint8_t SMS_CMGS_CMP(AT_Struct_Data *AT, char *sms_str_c) {
//	static uint8_t cmp_idx;
//	cmp_idx = 0;
//	for (int i = 0; i < 100; i++) {
//		if (AT->answer[i] == sms_str_c[i] && AT->answer[i] != '\0') {
//			cmp_idx++;
//		}
//	}
//	return cmp_idx;
//}

uint8_t SMS_ECHO_CMP(AT_Struct_Data *AT, char sms_str_c[]) {
	static uint8_t cmp_idx;
	cmp_idx = 0;
	for (int i = 0; i < 100; i++) {
		if (AT->answer[i] == sms_str_c[i] && AT->answer[i] > 48
				&& AT->answer[i] < 122) {
			cmp_idx++;
		}
	}
	return cmp_idx;
}

uint8_t NoAnswer(AT_Struct_Data *AT) {
	static uint8_t noanswer_idx;
	noanswer_idx = 0;
	for (int i = 0; i < 40; i++) {
		if (AT->answer[i] == '\0') {
			noanswer_idx++;
		}
	}
	return noanswer_idx;
}

uint8_t CMGS_SEARCH(AT_Struct_Data *AT) {
	for (int i = 0; i < 10; i++) {
		if (strncmp(&AT->answer[i], "CMGS", 4) == 0) {
			return 1;
		}
	}
	return 0;
}
/*Baterijos matavimas*/
uint16_t AT_Battery_meas(AT_Struct_Data *AT) {
	static char CRLF[] = "\r\n";
	static char part1[20];
	static char part2[20];
	static char part21[10];
	static char part22[10];
	static uint16_t battery;
	battery = 0;

	memset(part1, '\0', sizeof(part1));
	memset(part2, '\0', sizeof(part2));
	memset(part21, '\0', sizeof(part21));
	memset(part22, '\0', sizeof(part22));

	AT_SendWait(AT, "AT#CBC\r\n");

	char *token = strtok(AT->answer, CRLF);
	strcpy(part1, token);
	token = strtok(NULL, CRLF);
	strcpy(part2, token);

	*token = strtok(part2, ",");
	strcpy(part21, token);
	token = strtok(NULL, ",");
	strcpy(part22, token);

	battery = atoi(part22);
	return battery;
}
/*Žinutės interpretatorius*/
uint8_t SMS_Interpreter(Presetai_struct *params, AT_Struct_Data *AT,
		GPGGA_Struct_data *GNSS, GOOGLE_Struct *GOOGLE, char *sms_str_c) {
	static char part1[4];
	static char part2[4];
	static char *token;
	static uint16_t VBat_u16 = 0;
	static uint8_t syntax_on = 0;

	syntax_on = 0;

	if (strncmp(params->par.SOS_nr[0], AT->msg.content.number,
			strlen(params->par.SOS_nr[0])) == 0) {
		HAL_DBG_TRACE_MSG_COLOR("Phone number good\r\n",
				HAL_DBG_TRACE_COLOR_GREEN);
		if (strncmp(AT->msg.content.cmd, "PIN", 3) == 0
				&& strncmp(AT->msg.content.input, strcat(params->par.PIN, ", "),
						6) == 0 && strlen(AT->msg.content.input) == 10) {

			AT_SendWaitAskParams(AT, "AT+CREG?\r\n");

			token = strtok((char*) AT->msg.content.input, ", ");
			strcpy(part1, token);

			token = strtok(NULL, ", ");
			strcpy(part2, token);
			strcpy(params->par.PIN, part2);
			Set_presets_to_flash();
			memset(sms_str_c, '\0', 100);
			strcpy(sms_str_c, "Pin has been changed");
			syntax_on = 1;
		}

		/*Vietos siuntimas*/
		else if (strncmp(AT->msg.content.cmd, "LOCATION", 8) == 0) {
			memset(sms_str_c, '\0', 100);
			AT->gsm_gnss = GNSS_MODE;
			GNSS_GSM_SWITCH(AT);
			GNSS_Interpreter(AT, GNSS, GOOGLE);
			AT->gsm_gnss = GSM_MODE;
			GNSS_GSM_SWITCH(AT);
			AT_SendWaitAskParams(AT, "AT+CREG?\r\n");
			AT_SendWaitAskParams(AT, "AT+CSQ\r\n");

			sprintf(sms_str_c,
					"Current location \n%s=%.6f,%.6f ,\nSIGNAL RSSI: %d dBm",
					GOOGLE_HTTP, GOOGLE->lat, GOOGLE->lon,
					AT->network_params.rssi);
			HAL_DBG_TRACE_MSG_COLOR("Baigėsi\r\n", HAL_DBG_TRACE_COLOR_RED);
			syntax_on = 1;

		} else if (strncmp(AT->msg.content.cmd, "BATTERY", 8) == 0) {
			static uint8_t bat_pra = 0;
			static int16_t diff_VBat_MAX;
			bat_pra = 0;
			VBat_u16 = AT_Battery_meas(AT);
			AT_SendWaitAskParams(AT, "AT+CREG?\r\n");
			AT_SendWaitAskParams(AT, "AT+CSQ\r\n");

			memset(sms_str_c, '\0', 100);
			diff_VBat_MAX = VBat_u16 * 10 - BATTERY_MV_MIN;
			if (diff_VBat_MAX > 0) {
				bat_pra = 100 * (VBat_u16 * 10 - BATTERY_MV_MIN)
						/ (BATTERY_MV_MAX - BATTERY_MV_MIN);
				if (bat_pra > 100) {
					bat_pra = 100;
				}
			} else {
				bat_pra = 0;
			}
			sprintf(sms_str_c,
					"Battery level is %d %%, %d mV,\nSIGNAL RSSI: %d dBm",
					bat_pra, VBat_u16 * 10, AT->network_params.rssi);
			syntax_on = 1;
		} else if (strncmp(AT->msg.content.input, "06080423", 8) == 0) {
			HAL_DBG_TRACE_MSG_COLOR(
					"Baigėsi fondai kortelės arba limitas dienos pasiektas\r\n",
					HAL_DBG_TRACE_COLOR_RED);
			syntax_on = 0;
		} else {
			memset(sms_str_c, '\0', 100);
			strcpy(sms_str_c, "Error wrong command");
			syntax_on = 1;
		}
		if (syntax_on == 1) {
			memset(AT->answer, '\0', AT_ANSWER_SIZE);
#ifndef SMS_LIMIT
			SMS_sent(params, AT, sms_str_c, strlen(sms_str_c));
			HAL_DBG_TRACE_MSG_COLOR("Isiunciau\r\n", HAL_DBG_TRACE_COLOR_RED);
#endif
		}
	} else {
		HAL_DBG_TRACE_MSG_COLOR("Phone number bad\r\n", HAL_DBG_TRACE_COLOR_RED);
	}
	return 1;
}
/*GNSS interpretuotojas parsina žinutes, jeigu nesiunčia duomenų perkraut ir vėl bandyt, tikrinu herkono būseną jeigu atjungtų tai viską atjungt*/
extern uint8_t try_to_revive_gnss;
uint8_t GNSS_Interpreter(AT_Struct_Data *AT, GPGGA_Struct_data *GNSS,
		GOOGLE_Struct *GOOGLE) {
	static uint8_t gpgga_idx = 0;
	static uint8_t herkon_idx = 0;
	static uint8_t gpgga_empty = 0;
	gpgga_empty = 0;
	herkon_idx = 0;
	HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
	memset(GNSS, '\0', sizeof(GPGGA_Struct_data));
	while (GNSS->lat == 0 && gpgga_idx < GPS_fix_time) {
		HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
		memset(GNSS->raw, '\0', GNSS_RAW_SIZE);
		HAL_UART_Receive(&AT->hlpuart, (unsigned char*) GNSS->raw, 5,
		GPS_FIRST_MESSAGE_WAIT);
		HAL_UART_Receive(&AT->hlpuart, (unsigned char*) &GNSS->raw[5], 99,
		GPS_FULL_MESSAGE_WAIT);
		HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
		HAL_DBG_TRACE_PRINTF("%s, gpgga_idx=%d\n", GNSS->raw, gpgga_idx);
		GPS_Parser(GNSS, GOOGLE);
		gpgga_idx++;
		if (GNSS->raw[1] == '\0' && GNSS->raw[2] == '\0' && GNSS->raw[3] == '\0'
				&& GNSS->raw[4] == '\0' && GNSS->raw[5] == '\0'
				&& GNSS->raw[6] == '\0' && GNSS->raw[7] == '\0') {
			gpgga_empty++;
			if (gpgga_empty == 10) {
				gpgga_empty = 0;
				try_to_revive_gnss = 1;
			}
		} else {
			gpgga_empty = 0;
		}
		if (try_to_revive_gnss == 1) {
			try_to_revive_gnss = 0;
			AT->gsm_gnss = GNSS_MODE;
			GNSS_GSM_SWITCH(AT);
		}
		if (check_herkon == true) {
			Reed_input_sense();
			if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1) {
				herkon_idx++;
				if (herkon_idx == 2) {
					HAL_DBG_TRACE_MSG_COLOR("Herkonas off go sleep\r\n",
							HAL_DBG_TRACE_COLOR_RED);
					if (HAL_GPIO_ReadPin(SENSE_1V8_GPIO_Port, SENSE_1V8_Pin)
							== 1) {
						HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin,
								GPIO_PIN_RESET);
						HAL_Delay(4000);
						HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin,
								GPIO_PIN_SET);
						HAL_Delay(2000);
					}

					NVIC_SystemReset();
				}
			} else {
				herkon_idx = 0;
			}
		}
	}
	HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);
	gpgga_idx = 0;
	return 1;
}

/*kai skęsta kol nėra gnss tol ir nėra siuntimo */
uint8_t GNSS_Interpreter_Humidity_overload(AT_Struct_Data *AT,
		GPGGA_Struct_data *GNSS, GOOGLE_Struct *GOOGLE) {
	static uint8_t herkon_idx = 0;
	static uint8_t gpgga_empty = 0;
	gpgga_empty = 0;
	herkon_idx = 0;

	HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
	memset(GNSS, '\0', sizeof(GPGGA_Struct_data));
	while (GNSS->lat == 0) {
		HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
		memset(GNSS->raw, '\0', GNSS_RAW_SIZE);
		HAL_UART_Receive(&AT->hlpuart, (unsigned char*) GNSS->raw, 5,
		GPS_FIRST_MESSAGE_WAIT);
		HAL_UART_Receive(&AT->hlpuart, (unsigned char*) &GNSS->raw[5], 99,
		GPS_FULL_MESSAGE_WAIT);
		HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
		HAL_DBG_TRACE_PRINTF("%s\n", GNSS->raw);
		GPS_Parser(GNSS, GOOGLE);

		if (GNSS->raw[1] == '\0' && GNSS->raw[2] == '\0' && GNSS->raw[3] == '\0'
				&& GNSS->raw[4] == '\0' && GNSS->raw[5] == '\0'
				&& GNSS->raw[6] == '\0' && GNSS->raw[7] == '\0') {
			gpgga_empty++;
			if (gpgga_empty == 10) {
				gpgga_empty = 0;
				try_to_revive_gnss = 1;
			}
		} else {
			gpgga_empty = 0;
		}
		if (try_to_revive_gnss == 1) {
			try_to_revive_gnss = 0;
			AT->gsm_gnss = GNSS_MODE;
			GNSS_GSM_SWITCH(AT);
		}

		if (check_herkon == true) {
			Reed_input_sense();
			if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1) {
				herkon_idx++;
				if (herkon_idx == 2) {
					HAL_DBG_TRACE_MSG_COLOR("Herkonas off go sleep\r\n",
							HAL_DBG_TRACE_COLOR_RED);
					if (HAL_GPIO_ReadPin(SENSE_1V8_GPIO_Port, SENSE_1V8_Pin)
							== 1) {
						HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin,
								GPIO_PIN_RESET);
						HAL_Delay(4000);
						HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin,
								GPIO_PIN_SET);
						HAL_Delay(2000);
					}

					NVIC_SystemReset();
				}
			} else {
				herkon_idx = 0;
			}
		}

	}
	HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);
	return 1;
}

uint8_t GNSS_Interpreter_all_night_long(AT_Struct_Data *AT,
		GPGGA_Struct_data *GNSS, GOOGLE_Struct *GOOGLE) {
	static uint8_t gpgga_idx = 0;
	HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
	while (GNSS->lat == 0 && gpgga_idx < GPS_fix_time) {
//		memset(GNSS->raw, '\0', GNSS_RAW_SIZE);
		HAL_UART_Receive(&AT->hlpuart, (unsigned char*) GNSS->raw, 5,
		GPS_FIRST_MESSAGE_WAIT);
		HAL_UART_Receive(&AT->hlpuart, (unsigned char*) &GNSS->raw[5], 99,
		GPS_FULL_MESSAGE_WAIT);
		HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT->hlpuart);
		HAL_DBG_TRACE_PRINTF("%s, gpgga_idx=%d\n", GNSS->raw, gpgga_idx);
		GPS_Parser(GNSS, GOOGLE);
	}
	gpgga_idx = 0;
	return 1;
}
/*Baudrate keitimui, kai usart pjaudavo tai sumažinimui*/
uint8_t AT_change_baudrate(UART_HandleTypeDef *huart, AT_Struct_Data *AT) {
	static bool is_device_on = false;
	static bool baudrate_compatable = false;
	baudrate_compatable = false;
	is_device_on = false;

#if defined(BAUDRATE_9600)
	if (baudrate_compatable == false) {
		m_UART_CHANGE_BAUDRATE(huart, 9600);
		AT_SendWait(AT, "AT\r\n");
		if (AT_Parser(AT, m_OK_CHECK) == 1) {
			baudrate_compatable = true;
			is_device_on = true;
			return 1;
		}
	}

	if (baudrate_compatable == false) {
		m_UART_CHANGE_BAUDRATE(huart, 115200);
		AT_SendWait(AT, "AT\r\n");
		if (AT_Parser(AT, m_OK_CHECK) == 1) {
			AT_SendWait(AT, "AT+IPR=9600\r\n");
			m_UART_CHANGE_BAUDRATE(huart, 9600);
			AT_SendWait(AT, "AT&W0\r\n");
			HAL_Delay(100);
			baudrate_compatable = true;
			is_device_on = true;
			return 1;
		}
	}

	if (is_device_on == false) {
		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
		HAL_Delay(5000);
		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_RESET);
		HAL_Delay(5000);
		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
		HAL_Delay(2000);

		if (HAL_GPIO_ReadPin(SENSE_1V8_GPIO_Port, SENSE_1V8_Pin) != 1) {
			HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
			HAL_Delay(5000);
			HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_RESET);
			HAL_Delay(5000);
			HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
			HAL_Delay(2000);
		}
		is_device_on = true;
	}

	if (is_device_on == true) {
		m_UART_CHANGE_BAUDRATE(huart, 9600);
		AT_SendWait(AT, "AT\r\n");
		if (AT_Parser(AT, m_OK_CHECK) == 1) {
			baudrate_compatable = true;
			is_device_on = true;
			return 1;
		}

		m_UART_CHANGE_BAUDRATE(huart, 115200);
		AT_SendWait(AT, "AT\r\n");
		if (AT_Parser(AT, m_OK_CHECK) == 1) {
			AT_SendWait(AT, "AT+IPR=9600\r\n");
			m_UART_CHANGE_BAUDRATE(huart, 9600);
			AT_SendWait(AT, "AT&W0\r\n");
			HAL_Delay(100);
			baudrate_compatable = true;
			is_device_on = true;
			return 1;
		}

	}
#elif defined(BAUDRATE_115200)
	if (baudrate_compatable == false) {
		m_UART_CHANGE_BAUDRATE(huart, 115200);
		AT_SendWait(AT, "AT\r\n");
		if (AT_Parser(AT, m_OK_CHECK) == 1) {
			baudrate_compatable = true;
			is_device_on = true;
			return 1;
		}
	}

	if (baudrate_compatable == false) {
		m_UART_CHANGE_BAUDRATE(huart, 9600);
		AT_SendWait(AT, "AT\r\n");
		if (AT_Parser(AT, m_OK_CHECK) == 1) {
			AT_SendWait(AT, "AT+IPR=115200\r\n");
			m_UART_CHANGE_BAUDRATE(huart, 115200);
			AT_SendWait(AT, "AT&W0\r\n");
			baudrate_compatable = true;
			is_device_on = true;
			return 1;
		}
	}

	if (is_device_on == false) {
		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
		HAL_Delay(5000);
		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_RESET);
		HAL_Delay(5000);
		HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
		HAL_Delay(2000);

		if (HAL_GPIO_ReadPin(SENSE_1V8_GPIO_Port, SENSE_1V8_Pin) != 1) {
			HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
			HAL_Delay(5000);
			HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_RESET);
			HAL_Delay(5000);
			HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
			HAL_Delay(2000);
		}
		is_device_on = true;
	}

	if (is_device_on == true) {
		m_UART_CHANGE_BAUDRATE(huart, 115200);
		AT_SendWait(AT, "AT\r\n");
		if (AT_Parser(AT, m_OK_CHECK) == 1) {
			baudrate_compatable = true;
			is_device_on = true;
			return 1;
		}

		m_UART_CHANGE_BAUDRATE(huart, 9600);
		AT_SendWait(AT, "AT\r\n");
		if (AT_Parser(AT, m_OK_CHECK) == 1) {
			AT_SendWait(AT, "AT+IPR=115200\r\n");
			m_UART_CHANGE_BAUDRATE(huart, 115200);
			AT_SendWait(AT, "AT&W0\r\n");
			baudrate_compatable = true;
			is_device_on = true;
			return 1;
		}

	}
#endif

	return 0;
}

void led_double_blink(void) {
	static uint16_t time = 200;

	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	HAL_Delay(time);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	HAL_Delay(time);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	HAL_Delay(time);
	HAL_GPIO_TogglePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin);
	HAL_Delay(time);
}

/*resetėlis*/
void tellit_reset(AT_Struct_Data *AT) {
	HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);

	led_double_blink();
	HAL_Delay(1000);

	led_double_blink();
	HAL_Delay(1000);

	led_double_blink();
	HAL_Delay(1000);

	led_double_blink();
	HAL_Delay(1000);

	led_double_blink();

	GSM_GNSS_REBOOT(AT);

	AT->gsm_gnss = GSM_MODE;

	if (GNSS_Init(AT) != 1) {
		HAL_DBG_TRACE_MSG_COLOR("GNSS KLAIDA\r\n", HAL_DBG_TRACE_COLOR_RED);
		while (1) {
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
			HAL_Delay(200);
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
					GPIO_PIN_RESET);
			HAL_Delay(200);
		}
	}

	if (GSM_Init(AT) != 1) {
		HAL_DBG_TRACE_MSG_COLOR("GSM KLAIDA\r\n", HAL_DBG_TRACE_COLOR_RED);
		while (1) {
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
			HAL_Delay(200);
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
					GPIO_PIN_RESET);
			HAL_Delay(200);
		}
	}
}

