/*
 * flash.c
 *
 *  Created on: Mar 2, 2020
 *      Author: elect
 *     dedikuota User APP:
 */

#include "flash_presetai.h"

/*==========================================================================================================================================================*/
//															Naudojamos 2 funkcijos Set ir Get:
/*==========================================================================================================================================================*/

//--------------------------------------------------------------------------------------------------------------------------------------
// koeficientu nuskaitymas su apsauga.
void Get_presets_from_flash() {
	/* Nuskaitymas koeficientu is atminties */
	Read_from_flash((uint8_t*) &params, sizeof(Presetai_struct),
	FLASH_PRESETAI_ADDR);

	if (params.FlashOK_user != 0xB16B00B5) {

		params.FlashOK_user = 0xDEADBEEF;
		strcpy(params.par.PIN, "0000");
		Write_to_flash((uint8_t*) &params, sizeof(Presetai_struct),
		FLASH_PRESETAI_ADDR);
	}

}
//--------------------------------------------------------------------------------------------------------------------------------------
// suraso presetus i flash
void Set_presets_to_flash() {
	params.FlashOK_user = 0xB16B00B5;
	Write_to_flash((uint8_t*) &params, sizeof(Presetai_struct),
	FLASH_PRESETAI_ADDR);
}

/*Pasiima puslapio numerį*/
uint32_t GetPage(uint32_t Addr) {
	uint32_t page = 0;

	if (Addr < (FLASH_BASE + FLASH_BANK_SIZE)) {
		/* Bank 1 */
		page = (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;
	} else {
		/* Bank 2 */
		page = (Addr - (FLASH_BASE + FLASH_BANK_SIZE)) / FLASH_PAGE_SIZE;
	}

	return page;
}

//--------------------------------------------------------------------------------------------------------------------------------------

/*==========================================================================================================================================================*/
//															Darbines FLASH funkcijos:
/*==========================================================================================================================================================*/
uint8_t Write_to_flash(uint64_t *koef_array, uint16_t ilgis,
		uint32_t FlashAddress) {
	static uint32_t FirstPage = 0, NbOfPages = 0;
	// Trinimui
	FLASH_EraseInitTypeDef EraseInitStruct; // page erase structure
	uint32_t Erase_error = 0; // status variable
	uint32_t FirstSector = 0;
	HAL_StatusTypeDef status;
	// Rasymui:
	uint8_t Bloku_skc = (ilgis / 8) + 1;
	uint32_t iraseCnt2 = 0;	//
	uint16_t kiekPart = 0;	// kiek po 32 irase Bloku

	FirstPage = GetPage(FLASH_PRESETAI_ADDR);
	NbOfPages = GetPage(FLASH_PRESETAI_ADDR) + 1;
	/* Fill EraseInit structure*/
	EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	EraseInitStruct.Banks = FLASH_BANK_1;
	EraseInitStruct.Page = FirstPage;
	EraseInitStruct.NbPages = NbOfPages;

	status = HAL_FLASH_Unlock();
	// Trinti sektoriu
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP);
	__HAL_FLASH_CLEAR_FLAG(
			FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGSERR);
	status = HAL_FLASHEx_Erase(&EraseInitStruct, &Erase_error);

	while ((kiekPart < Bloku_skc)
			&& (FlashAddress <= (FLASH_USER_END_ADDR - WORD_LENGTH))) {
		if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FlashAddress,
				*(koef_array + iraseCnt2)) == HAL_OK) {
			status = 2;
		}
		iraseCnt2 += 1;	//kadangi duomenis paduodami kaip uint32_t nereikia cnt is 4 dalint papildomai !!
		FlashAddress += 8;	// flash adresas baitais skaiciuojasi
		kiekPart++;
	}
	HAL_FLASH_Lock();
	return status;
}
//--------------------------------------------------------------------------------------------------------------------------------------
void Read_from_flash(uint8_t *masyvas, uint16_t ilgis, uint32_t adresas) {

	for (uint16_t i = 0; i < ilgis; i++) {
		masyvas[i] = *(__IO uint8_t*) adresas;
		adresas = adresas + 1; // po 1 baitus skaito
	}
}

