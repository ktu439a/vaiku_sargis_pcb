/*
 * COM_debug.c
 *
 *  Created on: Jul 19, 2021
 *      Author: Lituotojas
 */
/* Uzsidefininti uart koks naudojamas h faile */

#include "COM_debug.h"

void printf_debug(uint8_t info[]) {
	HAL_UART_Transmit_DMA(&huart1, (uint8_t*) info, strlen(info));
}
