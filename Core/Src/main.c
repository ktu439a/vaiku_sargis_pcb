/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "comp.h"
#include "dma.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "def_module.h"
#include "AT_cmd.h"
#include "GNSS_lib.h"
#include "ansi_console.h"
#include "flash_presetai.h"
#include "stdbool.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
//#define DEBUG_REBOOT
#define ATIDAVIMUI
#define HUMIDITY_SENSOR
#define GREETINGS
#define HERKON_POWER
//#define GNSS_TEST
//#define GNSS_SMS_TEST
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/*
 * 1 GERAS
 * 2 ETALONAS
 * 3 beveik geras bsk patiunint
 * */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void Go_sleepy_sleepy(void);
void Get_drugged(void);
void Reed_sense(void);
//void Reed_input_sense(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t try_to_revive_gnss = 0;
bool check_herkon = false;

/*Herkono detektavimo funkcija prieš miegą ir proco resetėlis*/
void Reed_quick_shutdown(void) {
	if (check_herkon == true) {
		Reed_input_sense();
		if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1) {
			HAL_Delay(3000);
			if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1) {
				if (HAL_GPIO_ReadPin(SENSE_1V8_GPIO_Port, SENSE_1V8_Pin) == 1) {
					HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin,
							GPIO_PIN_RESET);
					HAL_Delay(4000);
					HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin,
							GPIO_PIN_SET);
					HAL_Delay(2000);
				}

				NVIC_SystemReset();
			}
		}
	}
}

bool periodic_alarm_gnss = false;
uint8_t dbg_idx = 0;
uint8_t dbg_error_idx = 0;
bool Humidity_overload_bool = 0;
bool Humidity_high_immediately = false;
uint8_t breakpoint = 0;
uint8_t msg_rx[10];
bool msg_received = false;

bool garbage_man = false;
uint8_t garbage_collection_idx = 0;
uint8_t garbage_how_much = 0;

bool call_when_ready = false;
bool dma_rx = false;

bool msg_sent = false;
Presetai_struct params;

uint8_t dbg_dma_receive = 0;

AT_Struct_Data AT;
GPGGA_Struct_data GNSS;
GOOGLE_Struct GOOGLE;
uint16_t VBat_u16 = 0;
int16_t diff_VBat_MAX = 0;
float VBat_f = 0;
char sms_str_c[100];
uint8_t dbg_daiktas = 0;
bool reset_routine = false;
uint8_t dbg_sms_rec = 0;
uint8_t dbg_received_SM = 0;
//bool HERKONAS_status = false;
uint8_t dbg_rtc = 0;
bool mcu_measure_bat = false;
uint8_t sms_ans_debug[255];
int volatile *const herkon_status = (int*) 0x10000000u;
uint8_t dbg_receved_msg = 0;
uint8_t m_AT = 0;
uint8_t m_CSQ = 0;
uint8_t m_SMS_var = 0;
uint8_t m_REBOOT = 0;
uint8_t timer_dbg = 0;

/*periodinis */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	timer_dbg++;
	periodic_alarm_gnss = true;
}

/*Ar žinutė gauta*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
//	HAL_ResumeTick();
	msg_received = true;
//	HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT.hlpuart);
	dbg_idx++;
}

uint8_t hal_comp2_level = 0;
uint8_t cmp_idx = 0;
void HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp) {
//	HAL_Delay(100);
	for (int i = 0; i < 65535; i++) {
		asm("NOP");
	}
	hal_comp2_level = HAL_COMP_GetOutputLevel(&hcomp2);
	dbg_daiktas++;
	if (hal_comp2_level == 0) {
		Humidity_overload_bool = true;
		cmp_idx++;
		HAL_DBG_TRACE_MSG_COLOR("Šlapia\r\n", HAL_DBG_TRACE_COLOR_GREEN);
		HAL_COMP_Stop(&hcomp2);
	}
//	HAL_COMP_Start(&hcomp2);

}
uint32_t err_code = 0;
/*KAI ERROR AdvancedInit Mask 255 nors turėtų būt 6*/
/*Error tikrint, jeigu ką usart perresetint*/
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart) {
//	HAL_DBG_TRACE_MSG_COLOR("BBD užpiso\r\n", HAL_DBG_TRACE_COLOR_GREEN);
//	HAL_DBG_TRACE_ARRAY("BBD užpiso\r\n",)
	err_code = HAL_UART_GetError(&huart2);

	if (err_code == 0) {
		HAL_DBG_TRACE_MSG_COLOR("HAL_UART_ERROR_NONE \r\n",
				HAL_DBG_TRACE_COLOR_GREEN);

	} else {

		if ((err_code & HAL_UART_ERROR_PE) == HAL_UART_ERROR_PE) {
			HAL_DBG_TRACE_MSG_COLOR("HAL_UART_ERROR_PE \r\n",
					HAL_DBG_TRACE_COLOR_GREEN);
		}
		if ((err_code & HAL_UART_ERROR_NE) == HAL_UART_ERROR_NE) {
			HAL_DBG_TRACE_MSG_COLOR("HAL_UART_ERROR_NE \r\n",
					HAL_DBG_TRACE_COLOR_GREEN);
		}

		if ((err_code & HAL_UART_ERROR_FE) == HAL_UART_ERROR_FE) {
			HAL_DBG_TRACE_MSG_COLOR("HAL_UART_ERROR_FE \r\n",
					HAL_DBG_TRACE_COLOR_GREEN);
		}

		if ((err_code & HAL_UART_ERROR_ORE) == HAL_UART_ERROR_ORE) {
			HAL_DBG_TRACE_MSG_COLOR("HAL_UART_ERROR_ORE \r\n",
					HAL_DBG_TRACE_COLOR_GREEN);
		}

		if ((err_code & HAL_UART_ERROR_DMA) == HAL_UART_ERROR_DMA) {
			HAL_DBG_TRACE_MSG_COLOR("HAL_UART_ERROR_DMA  \r\n",
					HAL_DBG_TRACE_COLOR_GREEN);
		}

		if ((err_code & HAL_UART_ERROR_RTO) == HAL_UART_ERROR_RTO) {
			HAL_DBG_TRACE_MSG_COLOR("HAL_UART_ERROR_RTO \r\n",
					HAL_DBG_TRACE_COLOR_GREEN);
		}
	}
	HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&huart2);
	HAL_UART_DeInit(&huart2);
	MX_USART2_UART_Init();

//	MX_LPUART1_UART_Init();
	/*naujai*/
//	__HAL_UART_CLEAR_OREFLAG(&huart2);
//	huart->RxState = HAL_UART_STATE_READY;
//
//
#if defined(WRITE_THROUGH) || defined(SMS_WRITE_FLASH)
	HAL_UART_Receive_DMA(&huart2, (uint8_t*) AT.answer, 6);
#endif
	dbg_error_idx++;

}
/*Herkono callback giliam miegui*/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

	if (GPIO_Pin == GPIO_PIN_0) {
		reset_routine = true;
		check_herkon = true;
		HAL_DBG_TRACE_MSG_COLOR("Issijungti\r\n", HAL_DBG_TRACE_COLOR_RED);
		Reed_input_sense();
	}
#if defined(SMS_GPIO_INT)
	if (GPIO_Pin == GPIO_PIN_11) {
//		msg_received = true;
		dbg_error_idx++;
		dbg_receved_msg++;
	}
#endif
}

/*rtc*/
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc) {
	dbg_rtc++;
}

char msg_tx[] = "AT\r\n";
char sms_copy[100];
uint8_t msg_rx[10];
uint8_t answer = 40;
uint8_t data_debug[100];

uint8_t error = 1;
float dbg_f = 0;
uint8_t read_dma = 0;
uint8_t debug_praejo = 0;
uint8_t dbg_call = 0;
MCU_bat_struct mcu_bat;
//AT+CSQ
//+CSQ: 22,3
//
//OK
//4 palydovai per puse
uint8_t m_read = 0;
uint8_t cmgs_idx = 0;
bool leisk_timer = false;
/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */
	MX_GPIO_Init();
	MX_USART1_UART_Init();
//#ifndef INIT_fix
//#define INIT_fix
//#error "Init here must be in INIT section delete"
//#endif
	/*herkono detektas dabar tiesiog prikiši ir veikia turėtų sensinti */
#if defined(HERKON_POWER)|defined(ATIDAVIMUI)
	if (__HAL_PWR_GET_FLAG(PWR_FLAG_WUF1) == SET) {
		HAL_DBG_TRACE_MSG_COLOR("Reset after wake up\r\n",
				HAL_DBG_TRACE_COLOR_RED);
		__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF1);
		HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
		HAL_Delay(1000 * 3);
		HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);
		Reed_input_sense();
		if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1) {
			HAL_DBG_TRACE_MSG_COLOR("Herkonas low\r\n", HAL_DBG_TRACE_COLOR_RED);
			if (HAL_GPIO_ReadPin(SENSE_1V8_GPIO_Port, SENSE_1V8_Pin) == 1) {
				HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
				HAL_Delay(5000);
				HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_RESET);
				HAL_Delay(5000);
				HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
				HAL_Delay(2000);
			}
			HAL_DBG_TRACE_MSG_COLOR("Long sleep\r\n", HAL_DBG_TRACE_COLOR_RED);
			Get_drugged();
		}
		HAL_Delay(100);
		Reed_sense();

		HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
		HAL_Delay(1000 * 3);
		HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);
	} else {
		HAL_DBG_TRACE_MSG_COLOR("Po reset2\r\n", HAL_DBG_TRACE_COLOR_RED);
		Reed_input_sense();
		HAL_Delay(1000 * 3);
		if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1) {
			if (HAL_GPIO_ReadPin(SENSE_1V8_GPIO_Port, SENSE_1V8_Pin) == 1) {
				HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
				HAL_Delay(5000);
				HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_RESET);
				HAL_Delay(5000);
				HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin, GPIO_PIN_SET);
				HAL_Delay(2000);
			}

			HAL_DBG_TRACE_MSG_COLOR("Long sleep 2\r\n", HAL_DBG_TRACE_COLOR_RED);
			HAL_Delay(100);
			Get_drugged();
		}

		HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
		HAL_Delay(1000 * 3);
		HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_RESET);

		Reed_sense();
	}
#endif
	/* USER CODE END SysInit */
	/*txd pa2 geltonas laidas ir tada apsisuka ir rx proco*/
	/*rxd */
	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART1_UART_Init();
	MX_DMA_Init();
	MX_USART2_UART_Init();
	MX_COMP2_Init();
	MX_ADC1_Init();
	MX_RTC_Init();
	MX_TIM1_Init();
	/* USER CODE BEGIN 2 */
	HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	memset(GNSS.raw, '\0', sizeof(GNSS.raw));
	memset(sms_str_c, '\0', sizeof(sms_str_c));
	memset(&AT, 0, sizeof(AT));
	memset(&GOOGLE, 0, sizeof(GOOGLE));
	memset(&AT.answer, '\0', sizeof(AT.answer));
	memset(&params, 1, sizeof(params));
	AT.hlpuart = huart2;

//	memcpy(AT.answer, "\r\n+CMTI: \"SM\",1\r\n", 17);
//	if (strncmp(AT.answer, "\"SM\"", 4) == 0) {

//	while (1) {
//	}
	/*Startuojam drėgmę*/
	HAL_GPIO_WritePin(HUM_VCC_GPIO_Port, HUM_VCC_Pin, GPIO_PIN_SET);
#ifdef HUMIDITY_SENSOR
	HAL_Delay(1000);
	HAL_COMP_Start(&hcomp2);
#endif

	HAL_DBG_TRACE_MSG_COLOR("Startyiha\r\n", HAL_DBG_TRACE_COLOR_RED);
//	if (AT_change_baudrate(&huart2, &AT) == 0) {
//		while (1) {
//			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
//			HAL_Delay(200);
//			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
//					GPIO_PIN_RESET);
//			HAL_Delay(200);
//		}
//	}
#ifdef DEBUG_REBOOT
	GSM_GNSS_REBOOT(&AT);
#endif

//	while (1) {
//		AT_SendWait(&AT, "AT\r\n");
//		HAL_Delay(100);
//		if (AT.answer[0] != 0) {
//			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
//		}
//	}
	Get_presets_from_flash();

	AT.gsm_gnss = GSM_MODE;

	if (GNSS_Init(&AT) != 1) {
		HAL_DBG_TRACE_MSG_COLOR("GNSS KLAIDA\r\n", HAL_DBG_TRACE_COLOR_RED);
		while (1) {
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
			HAL_Delay(200);
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
					GPIO_PIN_RESET);
			HAL_Delay(200);
		}
	}

	if (GSM_Init(&AT) != 1) {
		HAL_DBG_TRACE_MSG_COLOR("GSM KLAIDA\r\n", HAL_DBG_TRACE_COLOR_RED);
		while (1) {
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
			HAL_Delay(200);
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
					GPIO_PIN_RESET);
			HAL_Delay(200);
		}
	}

//	AT_SendWaitParseOKRepeat(&AT, "AT+CMGD=1,4\r\n");

	/*CIA TASTEAS*/
//	AT_SendWaitAskParams(&AT, "AT+CREG?\r\n");
//	AT_SendWait_DEBUG(&AT, "AT+CREG?\r\n");
//	AT_SendWait_DEBUG(&AT, "AT+CIREG?\r\n");
//	AT_SendWait_DEBUG(&AT, "AT+WS46?\r\n");
//	AT_SendWait_DEBUG(&AT, "AT#WS46?\r\n");
//	AT_SendWait_DEBUG(&AT, "AT+CEREG?\r\n");
//	AT_SendWait_DEBUG(&AT, "AT+CGREG?\r\n");
//	AT_SendWait_DEBUG(&AT, "AT+COPS?\r\n");
//	AT_SendWait_DEBUG(&AT, "AT+CGDCONT?\r\n");
//	while (1) {
//	}
	/*TESTAS BAIGIASI*/
	/*Istrint visas žinutes*/
	AT_SendWait(&AT, "AT+CMGD=1,4\r\n");
	if (AT_Parser(&AT, m_OK_CHECK) != 1) {
		debug_praejo = 1;
	}
#ifdef GREETINGS
	/*Tikrinimas ar pirmas įjungimas ar ne daikto*/
	if (params.FlashOK_user == 0xB16B00B5) {
		char sms_str_c[150];
		memset(sms_str_c, '\0', 100);

		AT_SendWaitAskParams(&AT, "AT+CREG?\r\n");
		AT_SendWaitAskParams(&AT, "AT+CSQ\r\n");

		static uint8_t bat_pra = 0;
		bat_pra = 0;
		VBat_u16 = AT_Battery_meas(&AT);
		memset(sms_str_c, '\0', 100);
		diff_VBat_MAX = VBat_u16 * 10 - BATTERY_MV_MIN;
		if (diff_VBat_MAX > 0) {
			bat_pra = 100 * (VBat_u16 * 10 - BATTERY_MV_MIN)
					/ (BATTERY_MV_MAX - BATTERY_MV_MIN);
			if (bat_pra > 100) {
				bat_pra = 100;
			}
		} else {
			bat_pra = 0;
		}
		/*Isiuncia paprastą žinutę, jog įsijungė arba kad drėgnas ir reikia nuvalyt tokiu atveju neveikia!!*/
		if (Humidity_overload_bool == true) {
			Humidity_high_immediately = true;
			Humidity_overload_bool = false;
			sprintf(sms_str_c,
					"HUMIDITY SENSOR IS ALREADY WET. PLEASE DRY IT. SYSTEM GOES IN SHUTDOWN MODE, BATTERY=%d %%, %d mV, SIGNAL RSSI=%d dBm",
					bat_pra, VBat_u16 * 10, AT.network_params.rssi);
		} else {
			sprintf(sms_str_c,
					"THE SYSTEM HAS BEEN TURNED ON, BATTERY=%d %%, %d mV, SIGNAL RSSI=%d dBm",
					bat_pra, VBat_u16 * 10, AT.network_params.rssi);
		}

		SMS_sent(&params, &AT, sms_str_c, strlen(sms_str_c));

	}
#endif

//	AT_SendWait(&AT, "AT+CREG?\r\n");
//	HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
	/*Cia tas tikrinasi, jog jeigu drėgmė yra nepriiminės datos iš telit*/
#if defined(WRITE_THROUGH) || defined(SMS_WRITE_FLASH)
	if (Humidity_high_immediately == false) {
		HAL_UART_Receive_DMA(&huart2, (uint8_t*) AT.answer, 6);
	}
#endif

#ifdef GNSS_TEST
HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&huart2);
AT.gsm_gnss = GNSS_MODE;
GNSS_GSM_SWITCH(&AT);
GNSS_Interpreter_all_night_long(&AT, &GNSS, &GOOGLE);
AT.gsm_gnss = GSM_MODE;
GNSS_GSM_SWITCH(&AT);
#endif
	HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 600,
	RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
	debug_praejo = 1;

//	AT_SendWait(&AT, "AT+CMGD=1,4\r\n");
//	if (AT_Parser(&AT, m_OK_CHECK) != 1) {
//		debug_praejo = 1;
//	}
	msg_received = false;
	reset_routine = false;
	Humidity_overload_bool = false;
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/*DEBUG AT komandos tikrint ar modulis gyvas (per debug)*/
		if (m_AT == 1) {
			m_AT = 0;
			memset(&AT.answer, '\0', sizeof(AT.answer));
			AT_SendWait(&AT, "AT\r\n");
		}
		/*DEBUG CSQ komandos tikrint ar yra ryšys (per debug)*/
		if (m_CSQ == 1) {
			m_CSQ = 0;
			memset(&AT.answer, '\0', sizeof(AT.answer));
			AT_SendWaitAskParams(&AT, "AT+CSQ\r\n");
			AT_SendWait(&AT, "AT+CREG?\r\n");
//			READ_CEREG(&AT);
//			memset(&AT.answer, '\0', sizeof(AT.answer));
//			AT_SendWait(&AT, "AT\r\n");
		}

		/*DEBUG CMGL komandos nuskaityt visas žinutes iš SIM kortos (per debug)*/
		if (m_read == 1) {
			m_read = 0;
			HAL_UART_Transmit(&AT.hlpuart, (uint8_t*) ("AT+CMGL=\"ALL\"\r\n"),
					strlen("AT+CMGL=\"ALL\"\r\n"), 5 * BAUDRATE_COEFF);
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT.hlpuart);
			HAL_UART_Receive(&AT.hlpuart, (uint8_t*) sms_ans_debug, 200,
					50 * BAUDRATE_COEFF);
			debug_praejo = 1;
		}

		/*DEBUG REBOOT komandos perkraut (per debug)*/
		if (m_REBOOT == 1) {

			m_REBOOT = 0;
			tellit_reset(&AT);

		}
		/*DEBUG SMS komandos išsiųst (per debug)*/
		if (m_SMS_var == 1) {
			m_SMS_var = 0;
			static uint8_t sms_str_char[] = "TEST\n";
//			sprintf(sms_str_c, "ALARM\n", GOOGLE_HTTP, GOOGLE.lat, GOOGLE.lon);
			SMS_sent(&params, &AT, sms_str_char, strlen(sms_str_char));

		}

		/*DEBUG CALL komandos išsiųst (per debug)*/
		if (dbg_call == 1) {
			dbg_call = 0;
			SOS_CALL(&params, &AT);
//			CALL_STATE(&AT);
//			memset(&AT.answer, '\0', sizeof(AT.answer));
//			HAL_UART_Receive(&huart2, (uint8_t*) AT.answer, 40, 40000);
//			HAL_Delay(1000);
		}

		if (mcu_measure_bat == true) {
			mcu_measure_bat = false;
			m_adc_measure_bat(&hadc1, &mcu_bat);
			if (mcu_bat.battery_proc < 30) {
				AT_SendWaitAskParams(&AT, "AT+CREG?\r\n");
				AT_SendWaitAskParams(&AT, "AT+CSQ\r\n");
				memset(sms_str_c, '\0', sizeof(sms_str_c));
				sprintf(sms_str_c,
						"Dangerous battery level=%d %%, %d mV, SIGNAL RSSI=%d dBm",
						mcu_bat.battery_proc, mcu_bat.battery_mv,
						AT.network_params.rssi);

				memset(&AT.answer, '\0', sizeof(AT.answer));
				SMS_sent(&params, &AT, sms_str_c, strlen(sms_str_c));
				HAL_DBG_TRACE_MSG_COLOR("Isiunciau baterija\r\n",
						HAL_DBG_TRACE_COLOR_RED);
			}
		}

		/*Cia, kai herkonas atjungiamas tai peresitinu modulį vėl pradėt paskui švariai darbą nueina į gilų miegą pradžioje*/
		if (reset_routine == true) {
			reset_routine = false;
			__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF1);
			HAL_DBG_TRACE_MSG_COLOR("Reset routine\r\n",
					HAL_DBG_TRACE_COLOR_RED);
			Reed_input_sense();
			HAL_Delay(3000);
			if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) != 1) {
				if (HAL_GPIO_ReadPin(SENSE_1V8_GPIO_Port, SENSE_1V8_Pin) == 1) {
					HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin,
							GPIO_PIN_RESET);
					HAL_Delay(4000);
					HAL_GPIO_WritePin(ON_OFF_GPIO_Port, ON_OFF_Pin,
							GPIO_PIN_SET);
					HAL_Delay(2000);
				}

				NVIC_SystemReset();
			}
			Reed_sense();
		}

		/*Žinučių priėmimas*/
		if (msg_received == true) {
			msg_received = false;

			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT.hlpuart);

#if defined(WRITE_THROUGH) || defined(SMS_WRITE_FLASH)
			HAL_UART_Receive(&huart2, (unsigned char*) &AT.answer[6], 100,
					100 * BAUDRATE_COEFF);
			HAL_DBG_TRACE_MSG_COLOR("Something was received\r\n",
					HAL_DBG_TRACE_COLOR_GREEN);
#endif
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
					GPIO_PIN_RESET);
			dbg_sms_rec++;

//			if (strncmp(AT.answer, "\"SM\"", 5) == 1) {
//				msg_received = false;
//			}

			if (strncmp(&AT.answer[10], "SM", 2) == 0) {
				dbg_received_SM++;
			}

			memcpy(sms_copy, AT.answer, 100);
			/*Žinutės echo surinkimas*/
			if (garbage_man == true) {
				garbage_collection_idx++;
				if (garbage_collection_idx == garbage_how_much) {
					garbage_collection_idx = 0;
					garbage_man = false;
					if (call_when_ready == true) {
						call_when_ready = false;
					}
				}
				/*CMGS, jog gerai viskas nuėjo*/
			} else if (strncmp(&AT.answer[2], "+CMGS", 5) == 0) {

				breakpoint = 1;
				cmgs_idx++;
				HAL_DBG_TRACE_MSG_COLOR("SMS is FINE\r\n",
						HAL_DBG_TRACE_COLOR_RED);
				/*Išsiunčiant klaida buvo*/
			} else if (strncmp(&AT.answer[2], "+CME", 4) == 0) {
				breakpoint = 1;
				HAL_DBG_TRACE_MSG_COLOR("CME error\r\n",
						HAL_DBG_TRACE_COLOR_RED);
				memset(sms_str_c, '\0', sizeof(sms_str_c));
				sprintf(sms_str_c, "Error please try again");
				SMS_sent(&params, &AT, sms_str_c, strlen(sms_str_c));
				while (1) {
					HAL_Delay(1000);
					HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
							GPIO_PIN_SET);
					HAL_Delay(1000);
					HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
							GPIO_PIN_RESET);
				}
				/*echo SMS*/
			} else if (SMS_ECHO_CMP(&AT, sms_str_c) > 10) {
				breakpoint = 1;
				HAL_DBG_TRACE_MSG_COLOR("Echo SMS\r\n", HAL_DBG_TRACE_COLOR_RED);
			} else {
				/*Išparsinti žinutęs*/
				HAL_DBG_TRACE_MSG_COLOR("AT_PARSER\r\n",
						HAL_DBG_TRACE_COLOR_RED);
#if defined(WRITE_THROUGH)
				if (AT_Parser(&AT, m_SMS) == 1)
#elif defined(SMS_WRITE_FLASH)

				if (AT_SendWaitParse_FlashSMS_CMTI(&AT) == 1)
#elif defined(SMS_GPIO_INT)
				if (AT_SendWaitParse_FlashSMS(&AT) == 1)
#endif
						{

					HAL_DBG_TRACE_MSG_COLOR("AT_PRAEJO\r\n",
							HAL_DBG_TRACE_COLOR_RED);
					/*Nustatyti admin numeri*/
					if ((strncmp(AT.msg.content.cmd, "PIN", 3) == 0)
							&& (strncmp(AT.msg.content.input, params.par.PIN, 4)
									== 0)
							&& strlen(AT.msg.content.input) == 4) {

						strcpy(params.par.SOS_nr[0], AT.msg.content.number);
						params.FlashOK_user = 0xB16B00B5;
						Set_presets_to_flash();

						static uint8_t bat_pra = 0;
						static int16_t diff_VBat_MAX;
						bat_pra = 0;
						VBat_u16 = AT_Battery_meas(&AT);
						AT_SendWaitAskParams(&AT, "AT+CREG?\r\n");
						AT_SendWaitAskParams(&AT, "AT+CSQ\r\n");

						memset(sms_str_c, '\0', 100);
						diff_VBat_MAX = VBat_u16 * 10 - BATTERY_MV_MIN;
						if (diff_VBat_MAX > 0) {
							bat_pra = 100 * (VBat_u16 * 10 - BATTERY_MV_MIN)
									/ (BATTERY_MV_MAX - BATTERY_MV_MIN);
							if (bat_pra > 100) {
								bat_pra = 100;
							}
						} else {
							bat_pra = 0;
						}

						memset(sms_str_c, '\0', sizeof(sms_str_c));
						sprintf(sms_str_c,
								"This number has become admin, Battery level is %d %%, %d mV,\nSIGNAL RSSI: %d dBm",
								bat_pra, VBat_u16 * 10, AT.network_params.rssi);

						memset(&AT.answer, '\0', sizeof(AT.answer));
#ifndef SMS_LIMIT
						SMS_sent(&params, &AT, sms_str_c, strlen(sms_str_c));
						HAL_DBG_TRACE_MSG_COLOR("Isiunciau\r\n",
								HAL_DBG_TRACE_COLOR_RED);
#endif
					}
					/*Change pin number*/
					else {
						SMS_Interpreter(&params, &AT, &GNSS, &GOOGLE,
								sms_str_c);
						memset(sms_str_c, '\0', sizeof(sms_str_c));
					}
				} else {
					HAL_DBG_TRACE_MSG_COLOR("Echo\r\n", HAL_DBG_TRACE_COLOR_RED);
					breakpoint = 1;
					HAL_DBG_TRACE_PRINTF("err code: %s\n", AT.answer);
				}

			}
			/*viskas padaryta tai vėl laukt ką gaus iš modulio*/
			msg_received = false;
			HAL_DBG_TRACE_MSG_COLOR("Receive DMA\r\n",
					HAL_DBG_TRACE_COLOR_GREEN);
#if defined(WRITE_THROUGH) || defined(SMS_WRITE_FLASH)
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT.hlpuart);
			memset(&AT.answer, '\0', sizeof(AT.answer));
			HAL_UART_Receive_DMA(&huart2, (uint8_t*) AT.answer, 6);
#endif
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
		}

		/*Drėgmė suveikė*/
		if (Humidity_overload_bool == true) {
			HAL_COMP_Stop(&hcomp2);
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(BOOM_MINI_GPIO_Port, BOOM_MINI_Pin, GPIO_PIN_SET);
			AT_SendWaitAskParams(&AT, "AT+CREG?\r\n");
			AT_SendWaitAskParams(&AT, "AT+CSQ\r\n");

//			HAL_COMP_Stop(&hcomp2);

			HAL_DBG_TRACE_MSG_COLOR("Humidity alarm\r\n",
					HAL_DBG_TRACE_COLOR_RED);
			/*Skambina ir tikrina ar skambutis baigėsi ar ne*/
			SOS_CALL(&params, &AT);
			CALL_STATE(&AT);

			Reed_quick_shutdown();
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&huart2);

			/*Perkonfigūruot į GNSS rėžimą*/
			AT.gsm_gnss = GNSS_MODE;
			GNSS_GSM_SWITCH(&AT);

			/*Paleidžiamas GNSS ieškojimas*/
			GNSS_Interpreter_Humidity_overload(&AT, &GNSS, &GOOGLE);

			/*GSM rėžimas įjungtas*/
			AT.gsm_gnss = GSM_MODE;
			GNSS_GSM_SWITCH(&AT);

			Reed_quick_shutdown();
			sprintf(sms_str_c, "Current location \n%s=%.6f,%.6f",
			GOOGLE_HTTP, GOOGLE.lat, GOOGLE.lon);
			HAL_DBG_TRACE_MSG_COLOR("Baigėsi\r\n", HAL_DBG_TRACE_COLOR_RED);

			memset(sms_str_c, '\0', sizeof(sms_str_c));
			sprintf(sms_str_c, "ALARM\n%s=%.6f,%.6f", GOOGLE_HTTP, GOOGLE.lat,
					GOOGLE.lon);

			msg_received = false;
			/*Išsiunčiam sms žinutę*/
#ifndef SMS_LIMIT
			SMS_sent(&params, &AT, sms_str_c, strlen(sms_str_c));
#endif
			HAL_DBG_TRACE_MSG_COLOR("Receive DMA\r\n",
					HAL_DBG_TRACE_COLOR_GREEN);

#if defined(WRITE_THROUGH) || defined(SMS_WRITE_FLASH)
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT.hlpuart);
			HAL_UART_Receive_DMA(&huart2, (uint8_t*) AT.answer, 6);
#endif
			Humidity_overload_bool = false;

			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
			/*Paleidžiamas timer, kuris suveikęs jungs periodic_alarm_gnss, kad nebūtų vėl skambinimo, o iškart ieškot */
			HAL_TIM_Base_Start_IT(&htim1);
		}

		if (periodic_alarm_gnss == true) {
			HAL_DBG_TRACE_MSG_COLOR("Pradėjau ieškot\r\n",
					HAL_DBG_TRACE_COLOR_RED);
			periodic_alarm_gnss = false;
			AT_SendWaitAskParams(&AT, "AT+CREG?\r\n");
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin,
					GPIO_PIN_RESET);
			HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&huart2);
			Reed_quick_shutdown();
			AT.gsm_gnss = GNSS_MODE;
			GNSS_GSM_SWITCH(&AT);
			GNSS_Interpreter_Humidity_overload(&AT, &GNSS, &GOOGLE);
			AT.gsm_gnss = GSM_MODE;
			GNSS_GSM_SWITCH(&AT);
			Reed_quick_shutdown();
			sprintf(sms_str_c, "Current location \n%s=%.6f,%.6f",
			GOOGLE_HTTP, GOOGLE.lat, GOOGLE.lon);
			HAL_DBG_TRACE_MSG_COLOR("Baigėsi\r\n", HAL_DBG_TRACE_COLOR_RED);

			memset(sms_str_c, '\0', sizeof(sms_str_c));
			sprintf(sms_str_c, "ALARM\n%s=%.6f,%.6f", GOOGLE_HTTP, GOOGLE.lat,
					GOOGLE.lon);

			msg_received = false;
#ifndef SMS_LIMIT
			SMS_sent(&params, &AT, sms_str_c, strlen(sms_str_c));
#endif
			HAL_GPIO_WritePin(DEBUG_LED_GPIO_Port, DEBUG_LED_Pin, GPIO_PIN_SET);
			HAL_TIM_Base_Stop_IT(&htim1);
			HAL_TIM_Base_Start_IT(&htim1);
			HAL_DBG_TRACE_MSG_COLOR("Pabaigiau\r\n", HAL_DBG_TRACE_COLOR_RED);
		}
#if defined(WRITE_THROUGH) || defined(SMS_WRITE_FLASH)
		HAL_UART_Receive_DMA(&huart2, (uint8_t*) AT.answer, 6);
#endif
		HAL_UART_STANDART_CLEAR_ERROR_FLAGS(&AT.hlpuart);
		/*Visada eit miegelio viską pabaigus*/
		Go_sleepy_sleepy();

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1)
			!= HAL_OK) {
		Error_Handler();
	}
	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI
			| RCC_OSCILLATORTYPE_MSI;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	RCC_OscInitStruct.MSICalibrationValue = 0;
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 40;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */
void Go_sleepy_sleepy(void) {
	/*Suspend Tick increment to prevent wakeup by Systick interrupt.
	 Otherwise the Systick interrupt will wake up the device within 1ms (HAL time base)*/
	HAL_SuspendTick();
	/* Enter Sleep Mode , wake up is done once jumper is put between PA.12 (Arduino D2) and GND */
	HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
//	HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
	/* Resume Tick interrupt if disabled prior to SLEEP mode entry */
	HAL_ResumeTick();
}

void Get_drugged(void) {

	HAL_GPIO_DeInit(GPIOA, GPIO_PIN_0);
	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
	HAL_NVIC_DisableIRQ(EXTI0_IRQn);

	/* Enable Power Control clock */
	__HAL_RCC_PWR_CLK_ENABLE();

	/* Enable ultra low power BOR and PVD supply monitoring */
//	  HAL_PWREx_EnableBORPVD_ULP();
	/* Disable all used wakeup sources:  */
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN2);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN3);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN4);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN5);

	/* Clear Standby flag */
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);

	/* Clear all related wakeup flags*/
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF1);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF2);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF3);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF4);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF5);

	/* Enable WakeUp Pin PWR_WAKEUP_PINx: */
	HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);

	/* Enter the Standby mode */
	HAL_PWR_EnterSTANDBYMode();
}

void Wakeup_init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/*Configure GPIO pin : Mygtukas_Pin */
	GPIO_InitStruct.Pin = GPIO_PIN_0;
//	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	HAL_NVIC_SetPriority(EXTI0_IRQn, 1, 0);
}

void Reed_sense(void) {
	HAL_GPIO_DeInit(GPIOA, GPIO_PIN_0);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN2);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN3);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN4);
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN5);

	/* Clear Standby flag */
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);

	/* Clear all related wakeup flags*/
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF1);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF2);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF3);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF4);
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF5);

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/*Configure GPIO pin : Mygtukas_Pin */
	GPIO_InitStruct.Pin = GPIO_PIN_0;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	HAL_NVIC_SetPriority(EXTI0_IRQn, 1, 0);
}

//void Reed_input_sense(void) {
//	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0);
//	HAL_GPIO_DeInit(GPIOA, GPIO_PIN_0);
//	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
//
//	/*Configure GPIO pin : Mygtukas_Pin */
//	GPIO_InitStruct.Pin = GPIO_PIN_0;
//	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//	GPIO_InitStruct.Pull = GPIO_NOPULL;
//	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//}
/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

