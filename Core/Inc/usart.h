/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file    usart.h
 * @brief   This file contains all the function prototypes for
 *          the usart.c file
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USART_H__
#define __USART_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */
/*Kiek laiko laukti kol priims zinute*/
#define BAUDRATE_9600
#if defined(BAUDRATE_9600)
#define BAUDRATE_COEFF 15
#elif defined(BAUDRATE_115200)
#define BAUDRATE_COEFF 1
#else
#error "BAUDRATE DEFINE UZSIMESK"
#define BAUDRATE_9600
#endif
#define AT_WAIT_TIME 10*BAUDRATE_COEFF
/* USER CODE END Includes */

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_USART1_UART_Init(void);
void MX_USART2_UART_Init(void);

/* USER CODE BEGIN Prototypes */
void HAL_UART_STANDART_CLEAR_ERROR_FLAGS(UART_HandleTypeDef *huart1);
void HAL_UART_STANDART_CLEAREVERYTHING_ERROR_FLAGS(UART_HandleTypeDef *huart1);
void m_UART_CHANGE_BAUDRATE(UART_HandleTypeDef *huart, uint32_t baudrate);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __USART_H__ */

